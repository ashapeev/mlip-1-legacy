# MLIP

Machine Learning of Interatomic Potentials (MLIP)
is a software being developed and mainted by the group of
Alexander Shapeev (Skoltech).
Please note that this version of the software is no longer supported, refer to
[https://gitlab.com/ashapeev/mlip-2](https://gitlab.com/ashapeev/mlip-2)
for the latest version.
For information on usage and redistribution, please refer to `LICENSE`.

## Quickstart guide

### Quick installation from Source
Download through git by executing
`git clone http://gitlab.skoltech.ru/shapeev/mlip.git`

### Compile
Type `make mlp` in the make folder for the mlp utility.
Run the shell script in make/LAMMPS to install MLIP with LAMMPS.
The lmp_serial and lmp_mpi will be created in the LAMMPS/src folder and copied
over to the make/ folder

### Run unit tests
Execute `../make/mlp self-test` from the `test/` folder

### Try out examples
See `doc/examples/readme.txt`

## Developers and Contributers
See `CONTRIBUTORS.md`.
