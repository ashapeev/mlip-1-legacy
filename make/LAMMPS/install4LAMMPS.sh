#!/bin/sh
 #   This software is called MLIP for Machine Learning Interatomic Potentials.
 #   MLIP can only be used for non-commercial research and cannot be re-distributed.
 #   The use of MLIP must be acknowledged by citing approriate references.
 #   See the LICENSE file for details.
 #
#   Contributors: Ivan Novikov

LAMMPS_PATH=$1

cd ..
MLIP_PATH=$(pwd)
make lib
cd LAMMPS/

mkdir -p $LAMMPS_PATH/lib/mlip
cp ../../lib/libmlip.a $LAMMPS_PATH/lib/mlip
cp Makefile.lammps.template $LAMMPS_PATH/lib/mlip/Makefile.lammps
cp -r ../../src/external/MLIP4LAMMPS/USER-MLIP/ $LAMMPS_PATH/src/
cp Install.sh $LAMMPS_PATH/src/USER-MLIP/
cp README $LAMMPS_PATH/src/USER-MLIP/
cd $LAMMPS_PATH/src
make no-user-mlip
make yes-user-mlip

make serial
make mpi
cp lmp_serial $HOME
cp lmp_mpi $HOME
cd
mv lmp_serial $MLIP_PATH/lmp_serial
mv lmp_mpi $MLIP_PATH/lmp_mpi
