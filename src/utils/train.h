/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin
 */

#ifndef MLIP_UTILS_TRAIN_H
#define MLIP_UTILS_TRAIN_H


#include "../linear_regression.h"
#ifdef MLIP_MPI
#	include "mpi.h"
#endif


#ifdef MLIP_MPI
void train(	const std::string& mtpfnm, 
			const std::string& cfgfnm, 
			double ene_eq_wgt = 1.0,
			double frc_eq_wgt = 0.001,
			double str_eq_wgt = 0.1,
			double relfrc_wgt = 0.0 )
{
	int mpi_rank=-1;
	int mpi_size=-1;

	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

	cout << "mpi_rank = " << mpi_rank << ", mpi_size = "
		<< mpi_size << ", MTP: " << mtpfnm << ", database: " << cfgfnm << endl;

	Configuration cfg;

	MPI_Barrier(MPI_COMM_WORLD);

	MTP MTP(mtpfnm);
	LinearRegression learner(&MTP, ene_eq_wgt, frc_eq_wgt, str_eq_wgt);
	learner.wgt_rel_forces = relfrc_wgt;

	ifstream ifs(cfgfnm);
	int proc_cntr=0;
	for (int cntr = 0; cfg.Load(ifs) == Configuration::VALID; cntr++)
	{
		if (cntr % mpi_size == mpi_rank)
		{
			double wgt = 1.0;///((cfg.energy/cfg.size()) + 7.7);
			learner.AddToSLAE(cfg, wgt);
			if (mpi_rank == 0)
			{
				cout << cntr << '\r';
				cout.flush();
			}
			proc_cntr++;
		}
	}
	ifs.close();
	cout << "process#" << mpi_rank << ": " << proc_cntr << " configuration processed\n";

	//learner.Train();
	//string fnm = "partialearned.mtp" + std::to_string(rank);
	//	MTP.Save(fnm.c_str());

	double* mtrx = NULL;
	double* rp = NULL;
	int n = MTP.alpha_count;

	if (mpi_rank == 0)
	{
		mtrx = new double[n*n];
		rp = new double[n];
		memset(mtrx, 0, n*n*sizeof(double));
		memset(rp, 0, n*sizeof(double));
	}

	MPI_Reduce(learner.quad_opt_matr, mtrx, n*n, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(learner.quad_opt_vec, rp, n, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	int eqncnt = 0;
	double scalar = 0.0;
	MPI_Reduce(&learner.quad_opt_eqn_count, &eqncnt, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&learner.quad_opt_scalar, &scalar, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (mpi_rank == 0)
	{
		memcpy(learner.quad_opt_matr, mtrx, n*n*sizeof(double));
		memcpy(learner.quad_opt_vec, rp, n*sizeof(double));
		learner.quad_opt_scalar = scalar;
		learner.quad_opt_eqn_count = eqncnt;

		ofstream ofs("mtrx.bin", ios::binary);
		ofs.write((char*)&n, sizeof(n));
		ofs.write((char*)&learner.quad_opt_eqn_count, sizeof(learner.quad_opt_eqn_count));
		ofs.write((char*)&learner.quad_opt_scalar, sizeof(double));
		ofs.write((char*)learner.quad_opt_vec, n*sizeof(double));
		ofs.write((char*)learner.quad_opt_matr, n*n*sizeof(double));
		ofs.close();

		delete[] mtrx;
		delete[] rp;

		learner.Train();

		MTP.Save(mtpfnm);
	}
}
#else
void train(	const std::string& mtpfnm, 
			const std::string& cfgfnm, 
			double ene_eq_wgt = 1.0,
			double frc_eq_wgt = 1.0,
			double str_eq_wgt = 1.0,
			double relfrc_wgt = 0.0 )
{

	Configuration cfg;

	MTP MTP(mtpfnm);
	LinearRegression learner(&MTP, ene_eq_wgt, frc_eq_wgt, str_eq_wgt);
	learner.wgt_rel_forces = relfrc_wgt;

	ifstream ifs(cfgfnm);
	int proc_cntr=0;
	for (int cntr = 0; cfg.Load(ifs) == Configuration::VALID; cntr++)
	{
		double wgt = 1.0;///((cfg.energy/cfg.size()) + 7.7);
		learner.AddToSLAE(cfg, wgt);
		cout << cntr << '\r';
		cout.flush();
		proc_cntr++;
	}
	ifs.close();

	int n = MTP.alpha_count;

	ofstream ofs("mtrx.bin", ios::binary);
	ofs.write((char*)&n, sizeof(n));
	ofs.write((char*)&learner.quad_opt_eqn_count, sizeof(learner.quad_opt_eqn_count));
	ofs.write((char*)&learner.quad_opt_scalar, sizeof(double));
	ofs.write((char*)learner.quad_opt_vec, n*sizeof(double));
	ofs.write((char*)learner.quad_opt_matr, n*n*sizeof(double));
	ofs.close();

	learner.Train();

	MTP.Save(mtpfnm);
}
#endif

#endif
