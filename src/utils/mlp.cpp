/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin, Ivan Novikov
 */


#include "../common/stdafx.h"
#include "../control_unit.h"
#include "train.h"
#include "calc_errors.h"
#include "self_test.h"
#include "mlp.h"



void self_test()
{
	ofstream logstream("temp/log");
	SetStreamForOutput(&logstream);

	int mpi_rank = 0;
	int mpi_size = 1;

#ifdef MLIP_MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif

#ifndef MLIP_DEBUG
	if (mpi_rank == 0) {
		std::cout << "Note: self-test is running without #define MLIP_DEBUG;\n"
			<< "      build with -DMLIP_DEBUG and run if troubles encountered" << std::endl;
	}
#endif

	if (mpi_rank == 0) {
		std::cout << "Serial tests:" << std::endl;
		if (!RunAllTests(false)) return;
	}
//#ifdef MLIP_MPI
//	if (mpi_rank == 0) std::cout << "MPI pub tests (" << mpi_size << " cores):" << std::endl;
//	RunAllTests(true);
//#endif // MLIP_MPI

	logstream.close();
}

int commands(const std::string& command,
	std::vector<std::string>& args,
	std::map<std::string, std::string>& opts)
{
	bool is_command_not_found = true;
	int mpi_rank = 0;
	int mpi_size = 1;
#ifdef MLIP_MPI
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
#endif

	if (command == "list" || (command == "help")) {
		is_command_not_found = false;
		if (mpi_rank == 0) {
			std::cout << "mlp "
#ifdef MLIP_MPI
				<< "mpi version (" << mpi_size << " cores)." << std::endl;
#else
				<< "serial version." << std::endl;
#endif
			if (command == "help" && args.size() == 0)
				std::cout << USAGE;
			if (command == "list")
				std::cout <<
				"Usage:\n"
				"mlp [command] [options/arguments]\n"
				"List of available commands:\n";
		}
	}

#include "mlp_commands"

	if (is_command_not_found)
		if (mpi_rank == 0) std::cout << "Error: command " << command << " does not exist." << std::endl;

	return 1;
}


int main(int argc, char *argv[])
{
#ifdef MLIP_MPI
	MPI_Init(&argc, &argv);
#endif

	std::vector<std::string> args;
	std::map<std::string, std::string> opts;

	// parse argv to extract args and opts
	std::string command = "help";
	if (argc >= 2) command = argv[1];
	ParseOptions(argc-1, argv+1, args, opts);

	try {
		commands(command, args, opts);
	}
	catch (const MlipException& e) { std::cerr << e.What(); return 1; }

#ifdef MLIP_MPI
	MPI_Finalize();
#endif

	return 0;
}
