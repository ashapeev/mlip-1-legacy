/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */


#ifndef MLIP_UTILS_CALC_ERRORS_H
#define MLIP_UTILS_CALC_ERRORS_H


#include "../error_monitor.h"
#include "../mtp.h"
#ifdef MLIP_MPI
#	include "mpi.h"
#endif


using namespace std;


#ifdef MLIP_MPI
void calc_errors(const string& mtpfnm, const string& cfgfnm)
{
	int mpi_rank = -1;
	int mpi_size = -1;

	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

	cout << "mpi_rank = " << mpi_rank << ", mpi_size = " << mpi_size << endl;

	Configuration cfg;

	MPI_Barrier(MPI_COMM_WORLD);

	MTP MTP(mtpfnm);
	ErrorMonitor errmon(0.0);
	Configuration cfg_orig;

	ifstream ifs(cfgfnm);
	int proc_cntr = 0;
	for (int cntr = 0; cfg.Load(ifs); cntr++)
	{
		if (cntr % mpi_size == mpi_rank)
		{
			cfg_orig = cfg;
			MTP.CalcEFS(cfg);
			errmon.collect(cfg_orig, cfg);
			if (mpi_rank == 0)
			{
				cout << cntr << '\r';
				cout.flush();
			}
			proc_cntr++;
		}
	}
	ifs.close();
	cout << "process#" << mpi_rank << ": " << proc_cntr << " configuration processed\n";

	ErrorMonitor bufferrmon;

	MPI_Reduce(&errmon.ene_all.max, &bufferrmon.ene_all.max, 5, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.ene_all.sum, &bufferrmon.ene_all.sum, 5, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.epa_all.max, &bufferrmon.epa_all.max, 5, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.epa_all.sum, &bufferrmon.epa_all.sum, 5, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.frc_all.max, &bufferrmon.frc_all.max, 5, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.frc_all.sum, &bufferrmon.frc_all.sum, 5, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.str_all.max, &bufferrmon.str_all.max, 5, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.str_all.sum, &bufferrmon.str_all.sum, 5, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.vir_all.max, &bufferrmon.vir_all.max, 5, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.vir_all.sum, &bufferrmon.vir_all.sum, 5, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.ene_all.count, &bufferrmon.ene_all.count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.epa_all.count, &bufferrmon.epa_all.count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.frc_all.count, &bufferrmon.frc_all.count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.str_all.count, &bufferrmon.str_all.count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	MPI_Reduce(&errmon.vir_all.count, &bufferrmon.vir_all.count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

	if (mpi_rank == 0)
	{
		bufferrmon.report(&cout);
	}
}
#else
void calc_errors(const string& mtpfnm, const string& cfgfnm)
{
	Configuration cfg;

	MTP MTP(mtpfnm);
	ErrorMonitor errmon(0.0);
	Configuration cfg_orig;

	ifstream ifs(cfgfnm);
	int proc_cntr = 0;
	for (int cntr = 0; cfg.Load(ifs); cntr++)
	{
		cfg_orig = cfg;
		MTP.CalcEFS(cfg);
		errmon.collect(cfg_orig, cfg);
		cout << cntr << '\r';
		cout.flush();
		proc_cntr++;
	}
	ifs.close();

	errmon.report(&cout);
}
#endif

#endif
