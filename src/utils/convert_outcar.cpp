/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#include "../ControlUnit.h"


std::ostream& logstrm = std::cerr;


using namespace std;


// argv[1] (optional) - full OUTCAR filename
// argv[2] (optional) - .cfgs filename
int main(int argc, char* argv[])
{
	std::ios::sync_with_stdio(false);

	string inpfnm = "OUTCAR";
	string outfnm = "converted.cfgs";

	if (argc > 1)
		inpfnm = argv[1];
	if (argc > 2)
		outfnm = argv[2];	
	
	Configuration cfg;
	cfg.LoadFromOUTCARFile(inpfnm);
	cfg.Save(outfnm);	

	return 0;
}


