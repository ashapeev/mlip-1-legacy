/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#ifndef MLIP_LAMMPS_INTERFACE_H
#define MLIP_LAMMPS_INTERFACE_H


#include "basic_potentials.h"


 // class allows one to use potentials embedded in LAMMPS. It is implemented via files exchange with LAMMPS that should be launched by MLIP for EFS calculation
class LAMMPS_potential : public AnyPotential
{
	const int io_max_attemps = 5;						// number of attempts to read/write files 

public:
	std::string input_fnm;								// input filename for LAMMPS with configuration (dump)
	std::string output_fnm;								// input filename for LAMMPS with configuration (dump)
	std::string start_command;							// command to OS environment launching LAMMPS

	LAMMPS_potential(	const string& _input_fnm,
						const string& _output_fnm,
						const string& _start_command) :
						input_fnm(_input_fnm),
						output_fnm(_output_fnm),
						start_command(_start_command) {}

	void CalcEFS(Configuration& conf);
};

#endif //#ifndef MLIP_LAMMPS_INTERFACE_H

