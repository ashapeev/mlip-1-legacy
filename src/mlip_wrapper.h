/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#ifndef MLIP_WRAPPER_H
#define MLIP_WRAPPER_H

#include "lotf.h"
#include "error_monitor.h"


// Class that allows working with MLIP in multiple regimes (e.g. fitting, EFS calculating, active learning, fitting error calculation, etc.)
class MLIP_Wrapper : public AnyPotential, public LogWriting, protected InitBySettings
{
  private:
	// settings	initialized by default values
	std::string mlip_fnm = "";				// file name, MLIP will be loaded from. MLIP is not required if empty
	bool enable_EFScalc = false;			// if true MLIP will calculate EFS
	bool enable_learn = false;				// if true MLIP fitting will be performed
	std::string mlip_fitted_fnm = "";		// filename for writing MLIP after fitting. Saving mlip is not required if empty
	double fit_ene_wgt = 1.0;				// energy equation weight in fitting procedure
	double fit_frc_wgt = 0.0;				// forces equation weight in fitting procedure
	double fit_str_wgt = 0.0;				// stress equation weight in fitting procedure
	double fit_rel_frc_wgt = 0.0;			// parameter enabling fitting to relative forces
	std::string fit_log = "";				// filename or "stdout" or "stderr" for fitting log output. No logging if empty
	bool enable_select = false;				// turns selection on of true
	double slct_trsh_init = 0.0000001;		// value for maxvol initiation
	double slct_trsh_slct = 0.001;			// selection threshold 
	double slct_trsh_swap = 0.0;			// selection threshold for continue swaps if cfg is selected. If 0.0 it will be set equal to slct_trsh_slct
	double slct_nbh_wgt = 0.0;				// site energy equation weight
	double slct_ene_wgt = 1.0;				// energy equation weight
	double slct_frc_wgt = 0.0;				// forces equation weight
	double slct_str_wgt = 0.0;				// stress equation weight
	std::string slct_ts_fnm = "";			// if not empty selected set will be saved to this file
	std::string slct_state_save_fnm = "";	// if not empty selection state will be saved to this file 
	std::string slct_state_load_fnm = "";	// if not empty selected set will be loaded from this file
	std::string slct_log = "";				// filename or "stdout" or "stderr" for selection log output. No logging if empty
	bool lotf_EFSviaMTP = true;				// if true CalcEFS() returns EFS calculated by MLIP only, else abinitio EFS will be returned for selected configurations
	std::string lotf_log = "";				// filename or "stdout" or "stderr" for lotf log output. No logging if empty
	std::string cfgs_fnm = "";				// filename for recording configurations (calculated by CalcEFS()). No configurations are recorded if empty
	int skip_saving_N_cfgs = 0;				// every skip_saving_N_cfgs+1 configuration will be saved (see previous option)
	bool monitor_errs = false;				// enables error monitoring (comparison of abinitio EFS and MLIP EFS and accumulation errors for averaging, in particular)
	std::string errmon_log = "";			// filename or "stdout" or "stderr" for error monitor log output. No logging if empty
	std::string log_output = "";			// filename or "stdout" or "stderr" for this object log output. No logging if empty

	Configuration cfg_valid;				// temporal for errmon

	int call_cntr = 0;						// number of EFS calcs

	void InitSettings()						// Sets correspondence between variables and setting names in settings file
	{
		MakeSetting(mlip_fnm,			"MLIP");
		MakeSetting(enable_EFScalc,		"MLIP:Calculate_EFS");
		MakeSetting(enable_learn,		"MLIP:Fit");
		MakeSetting(mlip_fitted_fnm,	"MLIP:Fit:Save");
		MakeSetting(fit_ene_wgt,		"MLIP:Fit:Energy_equation_weight");
		MakeSetting(fit_frc_wgt,		"MLIP:Fit:Forces_equation_weight");
		MakeSetting(fit_str_wgt,		"MLIP:Fit:Stress_equation_weight");
		MakeSetting(fit_rel_frc_wgt,	"MLIP:Fit:Relative_forces_weight");
		MakeSetting(fit_log,			"MLIP:Fit:Log");
		MakeSetting(enable_select,		"MLIP:Select");
		MakeSetting(slct_trsh_init,		"MLIP:Select:Threshold_init");
		MakeSetting(slct_trsh_slct,		"MLIP:Select:Threshold_slct");
		MakeSetting(slct_trsh_swap,		"MLIP:Select:Threshold_swap");
		MakeSetting(slct_nbh_wgt,		"MLIP:Select:Site_E_weight");
		MakeSetting(slct_ene_wgt,		"MLIP:Select:Energy_weight");
		MakeSetting(slct_frc_wgt,		"MLIP:Select:Forces_weight");
		MakeSetting(slct_str_wgt,		"MLIP:Select:Stress_weight");
		MakeSetting(slct_ts_fnm,		"MLIP:Select:Save_TS");
		MakeSetting(slct_state_save_fnm,"MLIP:Select:Save_state");
		MakeSetting(slct_state_load_fnm,"MLIP:Select:Load_state");
		MakeSetting(slct_log,			"MLIP:Select:Log");
		MakeSetting(lotf_log,			"MLIP:LOTF:Log");
		MakeSetting(lotf_EFSviaMTP,		"MLIP:LOTF:EFSviaMTP");
		MakeSetting(cfgs_fnm,			"MLIP:Write_cfgs");
		MakeSetting(skip_saving_N_cfgs,	"MLIP:Write_cfgs:Skip_N");
		MakeSetting(monitor_errs,		"MLIP:Check_errors");
		MakeSetting(errmon_log,			"MLIP:Check_errors:Log");
		MakeSetting(log_output,			"MLIP:Log");
	};
	void SetUpMLIP();	// Initiats object state according to settings

  public:
	AnyPotential* p_abinitio = nullptr;		// pointer to abinitio potential
	AnyLocalMLIP* p_mlip = nullptr;			// pointer to MLIP
	AnyTrainer* p_learner = nullptr;		// pointer to training object
	MaxvolSelection* p_selector = nullptr;	// pointer to selecting object
	LOTF* p_lotf = nullptr;					// pointer to LOTF object
	vector<Configuration> training_set;		// training set (for pure training regime)
	ErrorMonitor errmon;					// error monitoring object

	MLIP_Wrapper(std::map<std::string, std::string> _settings, AnyPotential* _p_abinitio=nullptr);
	virtual ~MLIP_Wrapper();
	
	void CalcEFS(Configuration& cfg);		// function that does work configured according settings file (for example may perform fitting, EFS calculation, error caclulation, LOTF, etc.)
	void CalcE(Configuration& cfg);
};

#endif

