/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#include "basic_drivers.h"

CfgReader::CfgReader(std::string _filename, AnyPotential * _p_potential)
{
	p_potential = _p_potential;
	filename = _filename;
}

CfgReader::CfgReader(AnyPotential * _p_potential, 
					 std::map<std::string, std::string>& settings)
{
	p_potential = _p_potential;

	InitSettings();
	ApplySettings(settings);
	PrintSettings();
}

void CfgReader::Run()
{
	if (filename.find(' ', 0) != std::string::npos)
		ERROR("Whitespaces in filename restricted");
	ifs.open(filename, ios::binary);
	if (!ifs.is_open())
		ERROR("Can't open file for reading configurations");
	else
		Message("Configurations will be read from file \"" + filename + "\"");

	SetLogStream(log_output);

	int cfgcntr = 0;
	while (cfg.Load(ifs) && cfgcntr<max_cfg_cnt)
	{
		cfgcntr++;
		cfg.features["from"] = "database:" + filename;

		logstrm << "Cfg reading: #" << cfgcntr 
				<< " size: " << cfg.size()
				<< ", EFS data: " 
				<< (cfg.has_energy() ? 'E' : ' ')
				<< (cfg.has_forces() ? 'F' : ' ') 
				<< (cfg.has_stresses() ? 'S' : '-')
				<< (cfg.has_site_energies() ? 'V' : '-')
				<< (cfg.has_charges() ? 'Q' : '-')
				<< std::endl;

		p_potential->CalcEFS(cfg);
	}
	Message("Reading configuration complete. " + to_string(cfgcntr) +
			" configurations read");
}
