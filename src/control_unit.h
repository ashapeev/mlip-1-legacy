/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#ifndef MLIP_CONTROLUNIT_H
#define MLIP_CONTROLUNIT_H


#include "mlip_wrapper.h"
#include "drivers/relaxation.h"

const std::map<std::string, std::string> EMPTY_STRING_MAP;

// class that sets up connection among ab-initio model, configuration driver and MLIP_wrapper
// it establishes connection according to settings file
class ControlUnit : InitBySettings
{
private:

	int abinitio_selector = 0;
	double lj_rmin = 2.0;
	double lj_scale = 1.0;
	double lj_cutoff = 5.0;
	std::string vasp_poscar = "POSCAR";
	std::string vasp_outcar = "OUTCAR";
	std::string vasp_start = "./vasp";
	std::string lammps_input = "dump.inp";
	std::string lammps_output = "dump.out";
	std::string lammps_start = "./lmp.sh";
	std::string mtp_fnm = "MTP.mtp";

	void InitSettings()
	{
		MakeSetting(abinitio_selector, "Abinitio");
		MakeSetting(lj_rmin, "Abinitio:LJ:r_min");
		MakeSetting(lj_scale, "Abinitio:LJ:scale");
		MakeSetting(lj_cutoff, "Abinitio:LJ:cutoff");
		MakeSetting(vasp_poscar, "Abinitio:VASP:POSCAR");
		MakeSetting(vasp_outcar, "Abinitio:VASP:OUTCAR");
		MakeSetting(vasp_start, "Abinitio:VASP:Start_command");
		MakeSetting(lammps_input, "Abinitio:LAMMPS:Input_file");
		MakeSetting(lammps_output, "Abinitio:LAMMPS:Output_file");
		MakeSetting(lammps_start, "Abinitio:LAMMPS:Start_command");
		MakeSetting(mtp_fnm, "Abinitio:MTP:Filename");
	}

	void LoadSettings(const std::string& filename);	// Reads settings file constructing settings structure
	void ReadCommandLine(int argc, char* argv[]);	// Reads additional settings from command line (settings from command line overwrites settings from the input file)

public:
	AnyPotential* p_abinitio;						// pointer to "ab-initio" potential
	MLIP_Wrapper* p_wrp;							// pointer to MLIP_Wrapper
	AnyDriver* p_driver;							// pointer to the driver

	std::map<std::string, std::string> settings;

	void SetUpAbInitioPotential();					// sets up a proper Abinitio model according to settings structure
	void SetUpMLIP();								// sets up a MLIP_wrapper according to settings structure
	void SetUpDriver(AnyPotential* MDPot);			// sets up a configuration driver according to settings structure

	ControlUnit(const std::string& stngs_filename,	// if atach_driver==false no driver will be set. In this case it should be done manuallly or in some another way
				bool atach_driver = false,			// the last parameter is additional settings from a command line
				std::map<std::string, std::string> extra_settings = EMPTY_STRING_MAP);
	~ControlUnit();

	void RunDriver();								// starts driver
};

#endif //MLIP_CONTROLUNIT_H
