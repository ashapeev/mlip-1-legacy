/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin
 */


#include "linear_regression.h"


using namespace std;


LinearRegression::LinearRegression(	AnyLocalMLIP* _p_mtp,
									double _wgt_eqtn_energy,
									double _wgt_eqtn_force,
									double _wgt_eqtn_stress,
									double _wgt_rel_forces) :
	AnyTrainer(_p_mtp, _wgt_eqtn_energy, _wgt_eqtn_force, _wgt_eqtn_stress, _wgt_rel_forces),
	p_mtp((MTP*)_p_mtp)
{
	int n = p_mtp->alpha_count;		// Matrix size and basis functions count

	Mtrx1 = new double[n*n];
	Mtrx2 = new double[n*n];
	RightPart1 = new double[n];
	RightPart2 = new double[n];

	quad_opt_vec = RightPart1;
	quad_opt_matr = Mtrx1;

	quad_opt_eqn_count = 0;

	ClearSLAE();
}

LinearRegression::~LinearRegression()
{
	if (Mtrx1 != nullptr) delete[] Mtrx1;
	if (Mtrx2 != nullptr) delete[] Mtrx2;
	if (RightPart1 != nullptr) delete[] RightPart1;
	if (RightPart2 != nullptr) delete[] RightPart2;
}

void LinearRegression::ClearSLAE()
{
	int n = p_mtp->alpha_count;		// Matrix size 

	quad_opt_eqn_count = 0;
	quad_opt_scalar = 0.0;
	memset(quad_opt_vec, 0, n * sizeof(double));
	memset(quad_opt_matr, 0, n * n * sizeof(double));
}

void LinearRegression::CopySLAE()
{
	int n = p_mtp->alpha_count;		// Matrix size 

	if (quad_opt_matr == Mtrx1)
	{
		memcpy(Mtrx2, Mtrx1, n*n*sizeof(double));
		memcpy(RightPart2, RightPart1, n*sizeof(double));
	}
	else if (quad_opt_matr == Mtrx2)
	{
		memcpy(Mtrx1, Mtrx2, n*n*sizeof(double));
		memcpy(RightPart1, RightPart2, n*sizeof(double));
	}
	else
		ERROR("Invalid internal state");
}

void LinearRegression::RestoreSLAE()
{
	if (quad_opt_matr == Mtrx1)
	{
		quad_opt_matr = Mtrx2;
		quad_opt_vec = RightPart2;
	}
	else if (quad_opt_matr == Mtrx2)
	{
		quad_opt_matr = Mtrx1;
		quad_opt_vec = RightPart1;
	}
	else
		ERROR("Invalid internal state");
}

void LinearRegression::AddToSLAE(Configuration& cfg, double weight)
{
	if (cfg.size() == 0)				// 
		return;

	int n = p_mtp->alpha_count;		// Matrix size and basis functions count

	double wgt_energy = wgt_eqtn_energy / cfg.size();
	double wgt_forces = wgt_eqtn_forces;
	double wgt_stress = wgt_eqtn_stress / cfg.size();

	if (((wgt_forces > 0) && cfg.has_forces()) ||
		((wgt_stress > 0) && cfg.has_stresses()))
		p_mtp->CalcEFSGrads(cfg, energy_cmpnts, forces_cmpnts, stress_cmpnts);
	else if ((wgt_energy > 0) && cfg.has_energy())
		p_mtp->CalcEnergyGrad(cfg, energy_cmpnts);

	if ((wgt_energy > 0) && cfg.has_energy())
	{
		for (int i = 0; i < n; i++)
			for (int j = i; j < n; j++)
				quad_opt_matr[i*n + j] += weight * wgt_energy * energy_cmpnts[i] * energy_cmpnts[j];

		for (int i = 0; i < n; i++)
			quad_opt_vec[i] += weight * wgt_energy * energy_cmpnts[i] * cfg.energy;
		
		quad_opt_scalar += weight * wgt_energy * cfg.energy * cfg.energy;

		quad_opt_eqn_count += (weight > 0) ? 1 : ((weight < 0) ? -1 : 0);
	}

	if ((wgt_forces > 0) && cfg.has_forces())
		for (int ind = 0; ind < cfg.size(); ind++)
		{
			double wgt = (wgt_rel_forces<=0.0) ?
				weight * wgt_forces :
				weight * wgt_forces * wgt_rel_forces / (cfg.force(ind).NormSq() + wgt_rel_forces);

			for (int a=0; a<3; a++)
				for (int i=0; i<n; i++)
				{
					for (int j=i; j<n; j++)
						quad_opt_matr[i*n + j] += 
							wgt * forces_cmpnts(ind, a, i) * forces_cmpnts(ind, a, j);

					quad_opt_vec[i] += wgt * forces_cmpnts(ind, a, i) * cfg.force(ind, a);
				}
		
			for (int a = 0; a < 3; a++)
				quad_opt_scalar += weight * wgt_forces * cfg.force(ind, a) * cfg.force(ind, a);

			quad_opt_eqn_count += 3 * ((weight > 0) ? 1 : ((weight < 0) ? -1 : 0));
		}

	if ((wgt_stress > 0) && cfg.has_stresses())
	{
		for (int a = 0; a < 3; a++)
			for (int b = 0; b < 3; b++)
			{
				for (int i = 0; i < n; i++)
				{
					for (int j = i; j < n; j++)
						quad_opt_matr[i*n + j] += 
							weight * wgt_stress * stress_cmpnts(a,b,i) * stress_cmpnts(a,b,j);

					quad_opt_vec[i] += 
						weight * wgt_stress * stress_cmpnts(a, b, i) * cfg.stresses[a][b];
				}

				quad_opt_scalar += weight * wgt_stress * cfg.stresses[a][b] * cfg.stresses[a][b];
			}

		quad_opt_eqn_count += 6 * ((weight > 0) ? 1 : ((weight < 0) ? -1 : 0));
	}
}

void LinearRegression::RemoveFromSLAE(Configuration & cfg)
{
	AddToSLAE(cfg, -1);
}

void LinearRegression::JoinSLAE(LinearRegression& to_add)
{
	if (p_mtp->alpha_count != to_add.p_mtp->alpha_count)
		ERROR("Incompatible SLAE sizes detected");
	
	for (int i=0; i<p_mtp->alpha_count*p_mtp->alpha_count; i++)
		quad_opt_matr[i] += to_add.quad_opt_matr[i];
	for (int i = 0; i < p_mtp->alpha_count; i++)
		quad_opt_vec[i] += to_add.quad_opt_vec[i];
	quad_opt_scalar += to_add.quad_opt_scalar;
	quad_opt_eqn_count += to_add.quad_opt_eqn_count;
}

void LinearRegression::SymmetrizeSLAE()
{
	int n = p_mtp->alpha_count;		// Matrix size

	for (int i = 0; i < n; i++) 
		for (int j = i + 1; j < n; j++) 
			quad_opt_matr[j*n + i] = quad_opt_matr[i*n + j];
}

void LinearRegression::SolveSLAE()
{
	int n = p_mtp->alpha_count;		// Matrix size
	
	p_mtp->regress_coeffs.resize(n);
	double* regress_coeffs = p_mtp->regress_coeffs.data();

	// Regularization (soft)
	for (int i=0; i<n; i++)
		quad_opt_matr[i*n + i] += 1e-13*quad_opt_matr[i*n + i];

	// Gaussian Elimination with leading column swaps
	// find column with maximal in modulus element
	for (int i=0; i<n-1; i++) {
		int j0 = i;
		double max_el = 0.0;
		for (int j=i; j<n; j++)
		{
			double foo = fabs(quad_opt_matr[i*n + i]);
			if (foo > max_el)
			{
				max_el = foo;
				j0 = j;
			}
		}
		if (j0 != i)// swap columns using regress_coeffs as buffer array
		{
			for (int k=0; k<n; k++)
				regress_coeffs[k] = quad_opt_matr[k*n + i];
			for (int k=0; k<n; k++)
				quad_opt_matr[k*n + i] = quad_opt_matr[k*n + j0];
			for (int k=0; k<n; k++)
				quad_opt_matr[k*n + j0] = regress_coeffs[k];
		}
		for (int j=i+1; j<n; j++) {
			double& m_ii = quad_opt_matr[i*n + i];
			m_ii = __max(m_ii, 1.0e-13);	// strong regularization
			double ratio = quad_opt_matr[j*n + i] / quad_opt_matr[i*n + i];
			for (int count=i; count<n; count++) 
				quad_opt_matr[j*n + count] -= ratio * quad_opt_matr[i*n + count];
			quad_opt_vec[j] -= ratio * quad_opt_vec[i];
		}
	}
	// Calculating regression coefficients
	regress_coeffs[n - 1] = quad_opt_vec[n - 1] / quad_opt_matr[(n - 1)*n + (n - 1)];
	for (int i=n-2; i>=0; i--) {
		double temp = quad_opt_vec[i];
		for (int j=i+1; j<n; j++) {
			temp -= (quad_opt_matr[i*n + j] * regress_coeffs[j]);
		}
		regress_coeffs[i] = temp / quad_opt_matr[i*n + i];
	}
}

void LinearRegression::Train(std::vector<Configuration>& train_set)
{
	logstrm << "\tFitting: training over " << train_set.size() << " configurations" << endl;

	ClearSLAE();

	int cntr = 1;
	for (Configuration& cfg : train_set)
	{
		AddToSLAE(cfg);
		logstrm << "\tFitting: Adding to SLAE cfg#" << cntr++ << endl;
	}
	logstrm << "\tFitting: training ";

	Train();

	logstrm << "complete                    " << endl; // whitespeses erases previous messages
}

void LinearRegression::Train()
{
	SymmetrizeSLAE();

	if (quad_opt_eqn_count < p_mtp->alpha_count)
		Warning("Fitting: Not enough equations for training. Eq. count = " + 
				std::to_string(quad_opt_eqn_count) + " , whereas number of MTP parameters = " +
				std::to_string(p_mtp->alpha_count));

	CopySLAE();
	SolveSLAE();
	RestoreSLAE();
}

