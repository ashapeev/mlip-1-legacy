/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#include "control_unit.h"
#include "vasp_potential.h"
#include "lammps_potential.h"
#include "mtp.h"
#include "pair_potentials.h"


using namespace std;


void ControlUnit::LoadSettings(const string& filename)
{
	ifstream ifs(filename);
	if (!ifs.is_open())
		ERROR((string)"Can't open input file " + filename);
	else
	{
		Message("Reading settings paramaters from " + filename + " file");

		string buff_key;
		ifs >> buff_key;
		while (!ifs.eof())
		{
			if (buff_key.substr(0, 1) != "#")
			{
				string buff_val = "";
				ifs >> buff_val;
				if (buff_val.substr(0, 1) != "#")
					if (settings.count(buff_key) > 0)
						settings[buff_key] = buff_val;
					else
						settings.emplace(buff_key, buff_val);
			}

			ifs.ignore(HUGE_INT, '\n');
			ifs >> buff_key;
		}
	}
}

void ControlUnit::ReadCommandLine(int argc, char* argv[])
{
	if ((argc-1)%2 == 1)
		ERROR("Wrong argument count!\n");

	for (int i=1; i<argc; i++) {
		string buff(argv[i++]);

		if (settings.find(buff) != settings.end()) 
		{
			settings[buff] = string(argv[i]);
			Message("Setting changed:\t" + buff + " -> " + settings[buff]);
		} 
		else
			Message("New settings parameter \"" + buff + "\"");
	}
}

void ControlUnit::SetUpAbInitioPotential()
{
	if (abinitio_selector == 0)
	{
		Message("No Ab-initio model is set");
		p_abinitio = nullptr;
	}
	else if (abinitio_selector == 1)
	{
		Message("Ab-initio EFS data is provided by driver");
		p_abinitio = new VoidPotential();
	}
	else if (abinitio_selector == 2)
	{
		Message("Ab-initio model: Liennard-Jones pair potential");
		p_abinitio = new LJ(lj_rmin, lj_scale, lj_cutoff);
	}
	else if (abinitio_selector == 3)
	{
		Message("Ab-initio model: DFT by VASP");
		p_abinitio = new VASP_potential(vasp_poscar, vasp_outcar, vasp_start);
	}
	else if (settings.at("Abinitio") == "4")
	{
		Message("Ab-initio model: potential through LAMMPS (defined in launch script)");
		p_abinitio = new LAMMPS_potential(lammps_input, lammps_output, lammps_start);
	}
	else if (settings.at("Abinitio") == "5")
	{
		Message("Ab-initio model: pre-learned MTP");
		p_abinitio = new MTP(mtp_fnm);
	}
	else
	{
		ERROR("Abinitio option must be in range 0 - 5");
		p_abinitio = nullptr;
	}
}

void ControlUnit::SetUpMLIP()
{
	p_wrp = new MLIP_Wrapper(settings, p_abinitio);
}

void ControlUnit::SetUpDriver(AnyPotential* MDPot)
{
	if (settings.at("Driver") == "0")
	{
		Message("No configuration driver or external driver is used");
		p_driver = nullptr;
	}
	else if (settings.at("Driver") == "1")
	{
		Message("Configuration driver: database");
		p_driver = new CfgReader(MDPot, settings);
	}
	else if (settings.at("Driver") == "2")
	{
		Message("Configuration driver: internal relaxation");
		p_driver = new Relaxation(MDPot, settings);
	}
	else
	{
		ERROR("Driver option must be in range 0 - 3");
		p_driver = nullptr;
	}
}

ControlUnit::ControlUnit(const string& filename, bool attach_driver, std::map<std::string, std::string> extra_settings) :
	p_driver(nullptr),
	p_wrp(nullptr)
{
	InitSettings();

	LoadSettings(filename);
	for (auto const &s : extra_settings)
		settings[s.first] = s.second;
	// ReadCommandLine(argc, argv);

	ApplySettings(settings);

	// preventing database erasing
	if (settings.count("Driver") > 0 &&
		settings.count("Driver:Database") > 0 &&
		settings.count("MLIP") > 0 &&
		settings.count("MLIP:Write_cfgs") > 0)
		if (settings["Driver"] == "2" &&
			settings["MLIP"] == "1" &&
			settings["MLIP:Write_cfgs"] == settings["Driver:Database"] && 
			!settings["MLIP:Write_cfgs"].empty())
			ERROR("The same configuration file specified for reading and writing");

	SetUpAbInitioPotential();
	SetUpMLIP();
	if (attach_driver)
		SetUpDriver(p_wrp);
}

ControlUnit::~ControlUnit()
{
	if (p_driver != nullptr)
		delete p_driver;

	if (p_wrp != nullptr)
		delete p_wrp;

	if (p_abinitio != nullptr)
		delete p_abinitio;

	settings.clear();
}

void ControlUnit::RunDriver()
{
	if (settings["Driver"] == "2")
	{
		if (settings.count("Driver:Relaxation:Input_cfgs_filename")==0)
			ERROR("Input .cfgs filename is not specified");
		
		Relaxation* p_rlx = (Relaxation*) p_driver;

		auto cfgs = LoadCfgs(settings["Driver:Relaxation:Input_cfgs_filename"]);
		for (auto& cfg : cfgs)
		{
			p_rlx->cfg = cfg;
			p_rlx->Run();
		}
	}
	else 
		p_driver->Run();
}

