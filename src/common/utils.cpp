/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin
 */

#include <iostream>
#include <string>
#include "utils.h"


//#ifdef _MSC_VER
//#pragma warning (disable: 4996) // disable unsafe functions warnings in VC
//#endif


std::ostream* p_os = &std::cout;


void mlip_ERROR(std::string str)
{
	std::cerr << str << std::endl;
	std::cout << str << std::endl;
	exit(1);
}

void Warning(const std::string& str)
{
	//std::cerr << "WARNING: " << str << std::endl;
	if (p_os != nullptr)
		*p_os << "WARNING: " << str << std::endl;
}

void Message(const std::string& str)
{
	if (p_os != nullptr)
		*p_os << str << std::endl;
}

std::ostream* SetStreamForOutput(std::ostream* _p_os)
{
	std::ostream* tmp = p_os;
	p_os = _p_os;
	return tmp;
}


std::string& mlip_string_escape(std::string& s) {
	std::size_t found = (std::size_t)-1;

	while ((found = s.find_first_of(" \t\n\r\\", found + 1)) != std::string::npos) {
		if (s[found] == ' ') s.replace(found, 1, "\\_");
		else if (s[found] == '\t') s.replace(found, 1, "\\t");
		else if (s[found] == '\n') s.replace(found, 1, "\\n");
		else if (s[found] == '\r') s.replace(found, 1, "\\r");
	}
	return s;
}

std::string& mlip_string_unescape(std::string& s) {
	std::size_t found = (std::size_t)-1;

	while ((found = s.find_first_of("\\", found + 1)) != std::string::npos) {
		if (s[found + 1] == '_') s.replace(found, 2, " ");
		else if (s[found + 1] == 't') s.replace(found, 2, "\t");
		else if (s[found + 1] == 'n') s.replace(found, 2, "\n");
		else if (s[found + 1] == 'r') s.replace(found, 2, "\r");
	}
	return s;
}

LogWriting::LogWriting(std::ostream * _p_os) : null_stream(&null_buffer)
{
	SetLogStream(_p_os);
}

LogWriting::LogWriting(const std::string & filename) : null_stream(&null_buffer)
{
	SetLogStream(filename);
}

void LogWriting::SetLogStream(std::ostream * _p_os)
{
	if (delete_p_logstream)
		delete p_logstream;
	p_logstream = _p_os;
}

void LogWriting::SetLogStream(const std::string & filename)
{
	if (delete_p_logstream)
		delete p_logstream;
	if (filename.empty())
		p_logstream = nullptr;
	else if (filename == "stdout")
		SetLogStream(&std::cout);
	else if (filename == "stderr")
		SetLogStream(&std::cerr);
	else
	{
		p_logstream = new std::ofstream(filename);
		if (!((std::ofstream*)p_logstream)->is_open())
			ERROR("Can't open file \"" + filename + "\" for logging");
		delete_p_logstream = true;
	}
}

std::ostream * LogWriting::GetLogStream()
{
	return p_logstream;
}

LogWriting::~LogWriting()
{
	if (delete_p_logstream)
		delete p_logstream;
}

void InitBySettings::MakeSetting(bool & parameter, const std::string & setting_name)
{
	connector_bool.emplace(setting_name, &parameter);
}

void InitBySettings::MakeSetting(int & parameter, const std::string & setting_name)
{
	connector_int.emplace(setting_name, &parameter);
}

void InitBySettings::MakeSetting(double & parameter, const std::string & setting_name)
{
	connector_double.emplace(setting_name, &parameter);
}

void InitBySettings::MakeSetting(std::string & parameter, const std::string & setting_name)
{
	connector_string.emplace(setting_name, &parameter);
}

void InitBySettings::ApplySettings(std::map<std::string, std::string> settings)
{
	try
	{
		for (auto& stng : connector_bool)
			if (settings.count(stng.first) != 0 && !settings[stng.first].empty())
			{
				if (settings[stng.first] == "false" || 
					settings[stng.first] == "False" ||
					settings[stng.first] == "FALSE" ||
					settings[stng.first] == "0")
					*connector_bool[stng.first] = false;
				else if(settings[stng.first] == "true" ||
						settings[stng.first] == "True" ||
						settings[stng.first] == "TRUE" ||
						settings[stng.first] == "1")
					*connector_bool[stng.first] = true;
				else 
					ERROR("Can't interpret value \"settings[stng.first]\"\
							of boolean variable \"stng.first\"");
			}
		for (auto& stng : connector_int)
			if (settings.count(stng.first) != 0 && !settings[stng.first].empty())
				*connector_int[stng.first] = stoi(settings[stng.first]);
		for (auto& stng : connector_double)
			if (settings.count(stng.first) != 0 && !settings[stng.first].empty())
				*connector_double[stng.first] = stod(settings[stng.first]);
		for (auto& stng : connector_string)
			if (settings.count(stng.first) != 0 && !settings[stng.first].empty())
				*connector_string[stng.first] = settings[stng.first];
	}
	catch (...)
	{
		ERROR("Cannot parse settings");
	}
}

void InitBySettings::PrintSettings()
{
	for (auto& stng : connector_bool)
		Message('\t' + stng.first + " = " + (*stng.second ? "true" : "false"));
	for (auto& stng : connector_int)
		Message('\t' + stng.first + " = " + std::to_string(*stng.second));
	for (auto& stng : connector_double)
		Message('\t' + stng.first + " = " + std::to_string(*stng.second));
	for (auto& stng : connector_string)
		if (!stng.second->empty())
			Message('\t' + stng.first + " = " + *stng.second);
}

void ParseOptions(int _argc, char *_argv[], std::vector<std::string>& args,
	std::map<std::string, std::string>& opts)
{
	bool expect_options = true;

	for (int i = 1; i < _argc; i++) {
		std::string word = _argv[i];
		if (word[0] == '-' && expect_options) {
			if (word.length() == 1) ERROR("'-' is not a valid option");
			if (word[1] != '-') ERROR("option " + _argv[i] + " should start with '--', not '-'");
			if (word.length() == 2) {
				// this is '--'. Switch off expect_options
				expect_options = false;
			} else {
				size_t eq_pos = word.find_first_of("=");
				if (eq_pos == std::string::npos)
					opts[word.substr(2)] = "true";
				else
					opts[word.substr(2, eq_pos - 2)] = word.substr(eq_pos + 1);
			}
		} else
			args.push_back(_argv[i]);
	}
}
