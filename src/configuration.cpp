/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Alexander Shapeev, Evgeny Podryabinkin, Ivan Novikov
 */

#include <sstream>
#include <fstream>
#include <iomanip>
#include <set>
#include <iostream>
#include <algorithm>
#include "configuration.h"


using namespace std;


long Configuration::id_cntr = 0;

void Configuration::destroy()
{
	//lattice[0][0] = lattice[0][1] = lattice[0][2]
	// = lattice[1][0] = lattice[1][1] = lattice[1][2]
	// = lattice[2][0] = lattice[2][1] = lattice[2][2] = 0.0;
	types_.clear();
	pos_.clear();
	forces_.clear();
	site_energies_.clear();
	charges_.clear();
	has_energy_ = false;
	has_stresses_ = false;
	features.clear();

	set_new_id();	// new id will be set if configuration is reinitialized
}

bool Configuration::operator==(const Configuration& cfg) const
{
	if (lattice != cfg.lattice) return false;
	if (pos_ != cfg.pos_) return false;

	if (has_energy() != cfg.has_energy()) return false;
	if (has_energy() && energy != cfg.energy) return false;

	if (has_stresses() != cfg.has_stresses()) return false;
	if (has_stresses() && stresses != cfg.stresses) return false;

	if (has_forces() != cfg.has_forces()) return false;
	if (has_forces() && forces_ != cfg.forces_) return false;

	if (has_site_energies() != cfg.has_site_energies()) return false;
	if (has_site_energies() && site_energies_ != cfg.site_energies_) return false;

	if (has_charges() != cfg.has_charges()) return false;
	if (has_charges() && charges_ != cfg.charges_) return false;

	if (types_ != cfg.types_) return false;

	return true;
}

bool Configuration::operator!=(const Configuration& cfg) const
{
	return !operator==(cfg);
}

void Configuration::deform(const Matrix3& mapping)
{
	lattice *= mapping;

	for (Vector3& coord : pos_)
		coord = coord * mapping;
}

void Configuration::MoveAtomsIntoCell()
{
	const Matrix3 map_to_lattice_basis = lattice.inverse();

	for (int i=0; i<(int)size(); i++)
	{
		Vector3 pos_in_lattice_basis = pos_[i] * map_to_lattice_basis;
		
		for (int a=0; a<3; a++)
			pos_in_lattice_basis[a] -= floor(pos_in_lattice_basis[a]);

		pos_[i] = pos_in_lattice_basis * lattice;
	}
}

void Configuration::CorrectSupercell()
{
	// 1. Make the lattice as orthogonal as possible
	bool proceed=true;
	while (proceed)
	{
		proceed = false;
		for (int i=0; i<3; i++)
		{
			Vector3 l_i = Vector3(lattice[i][0], lattice[i][1], lattice[i][2]);
			for (int j=0; j<3; j++)
				if (i != j)
				{
					Vector3 l_j = Vector3(lattice[j][0], lattice[j][1], lattice[j][2]);
					double orig_l_j_norm2 = l_j.NormSq();
					double new_l_j_norm2 = (l_j + l_i).NormSq();
					for (int itr=1; new_l_j_norm2 < orig_l_j_norm2; itr++)
					{
						l_j += l_i;
						new_l_j_norm2 = (l_j + l_i).NormSq();
						proceed = true;
					}
					new_l_j_norm2 = (l_j - l_i).NormSq();
					for (int itr=1; new_l_j_norm2 < orig_l_j_norm2; itr++)
					{
						l_j -= l_i;
						new_l_j_norm2 = (l_j - l_i).NormSq();
						proceed = true;
					}

					lattice[j][0] = l_j[0];
					lattice[j][1] = l_j[1];
					lattice[j][2] = l_j[2];
				}
		}
	}

	// 2. Update atom positions
	MoveAtomsIntoCell();

	// 3. Make lattice matrix lower triangle
	Matrix3 Q;
	Matrix3 L;
	lattice.transpose().QRdecomp(Q, L);
	deform(Q);

	// 4. Set zero triangle instead of small numbers
	lattice[0][1] = 0.0;
	lattice[0][2] = 0.0;
	lattice[1][2] = 0.0;

	// 5. Rotate forces if present
	if (has_forces())
		for (Vector3& force : forces_)
			force = force * Q;

	// 6. Rotate stresses if present
	if (has_stresses())
		stresses = Q.transpose() * stresses * Q;
}

Configuration::Configuration() :
	has_energy_(false),
	has_stresses_(false),
	lattice(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0),
	stresses(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
{
	set_new_id();
}

Configuration::Configuration(	CrystalSystemType crystal_system, 
								double lattice_parameter, 
								int replication_times)
								// TODO: basic_lattice -> deformation mapping
{
	set_new_id();

	if (crystal_system == CrystalSystemType::SC)
	{
		Matrix3 basic_lattice = Matrix3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0); 
		basic_lattice *= lattice_parameter;
		lattice = basic_lattice * replication_times;

		for (int i = 0; i < replication_times; i++)
			for (int j = 0; j < replication_times; j++)
				for (int k = 0; k < replication_times; k++)
				{
					pos_.emplace_back();

					pos_.back()[0] = i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] = i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] = i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];
				}
	}
	else if (crystal_system == CrystalSystemType::BCC)
	{
		Matrix3 basic_lattice = Matrix3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
		basic_lattice *= lattice_parameter;
		lattice = basic_lattice * replication_times;

		for (int i = 0; i < replication_times; i++)
			for (int j = 0; j < replication_times; j++)
				for (int k = 0; k < replication_times; k++)
				{
					pos_.emplace_back();

					pos_.back()[0] = i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] = i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] = i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];

					pos_.emplace_back();

					pos_.back()[0] = 0.5 * (basic_lattice[0][0] + basic_lattice[1][0] + basic_lattice[2][0]);
					pos_.back()[1] = 0.5 * (basic_lattice[0][1] + basic_lattice[1][1] + basic_lattice[2][1]);
					pos_.back()[2] = 0.5 * (basic_lattice[0][2] + basic_lattice[1][2] + basic_lattice[2][2]);

					pos_.back()[0] += i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] += i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] += i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];
				}
	}
	else if (crystal_system == CrystalSystemType::FCC)
	{
		Matrix3 basic_lattice = Matrix3(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
		basic_lattice *= lattice_parameter;
		lattice = basic_lattice * replication_times;

		for (int i = 0; i < replication_times; i++)
			for (int j = 0; j < replication_times; j++)
				for (int k = 0; k < replication_times; k++)
				{
					pos_.emplace_back();

					pos_.back()[0] = i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] = i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] = i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];

					pos_.emplace_back();

					pos_.back()[0] = 0.5 * (0.0					+ basic_lattice[1][0] + basic_lattice[2][0]);
					pos_.back()[1] = 0.5 * (basic_lattice[0][1] + basic_lattice[1][1] + basic_lattice[2][1]);
					pos_.back()[2] = 0.5 * (basic_lattice[0][2] + basic_lattice[1][2] + basic_lattice[2][2]);

					pos_.back()[0] += i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] += i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] += i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];

					pos_.emplace_back();

					pos_.back()[0] = 0.5 * (basic_lattice[0][0] + basic_lattice[1][0] + basic_lattice[2][0]);
					pos_.back()[1] = 0.5 * (basic_lattice[0][1] + 0.0				  + basic_lattice[2][1]);
					pos_.back()[2] = 0.5 * (basic_lattice[0][2] + basic_lattice[1][2] + basic_lattice[2][2]);

					pos_.back()[0] += i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] += i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] += i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];

					pos_.emplace_back();

					pos_.back()[0] = 0.5 * (basic_lattice[0][0] + basic_lattice[1][0] + basic_lattice[2][0]);
					pos_.back()[1] = 0.5 * (basic_lattice[0][1] + basic_lattice[1][1] + basic_lattice[2][1]);
					pos_.back()[2] = 0.5 * (basic_lattice[0][2]	+ basic_lattice[1][2] + 0.0);

					pos_.back()[0] += i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] += i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] += i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];
				}
	}
	else if (crystal_system == CrystalSystemType::HCP)
	{
		Matrix3 basic_lattice = Matrix3(1.0, 0.0, 0.0, -0.5, sqrt(3)/2.0, 0.0, 0.0, 0.0, 2*sqrt(2.0/3.0));
		basic_lattice *= lattice_parameter;
		lattice = basic_lattice * replication_times;

		for (int i = 0; i < replication_times; i++)
			for (int j = 0; j < replication_times; j++)
				for (int k = 0; k < replication_times; k++)
				{
					pos_.emplace_back();

					pos_.back()[0] = i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] = i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] = i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];

					pos_.emplace_back();

					pos_.back()[0] = 2.0/3.0 * basic_lattice[0][0] + 1.0/3.0 * basic_lattice[1][0] + 0.5 * basic_lattice[2][0];
					pos_.back()[1] = 2.0/3.0 * basic_lattice[0][1] + 1.0/3.0 * basic_lattice[1][1] + 0.5 * basic_lattice[2][1];
					pos_.back()[2] = 2.0/3.0 * basic_lattice[0][2] + 1.0/3.0 * basic_lattice[1][2] + 0.5 * basic_lattice[2][2];

					pos_.back()[0] += i * basic_lattice[0][0] + j * basic_lattice[1][0] + k * basic_lattice[2][0];
					pos_.back()[1] += i * basic_lattice[0][1] + j * basic_lattice[1][1] + k * basic_lattice[2][1];
					pos_.back()[2] += i * basic_lattice[0][2] + j * basic_lattice[1][2] + k * basic_lattice[2][2];
				}
	}
	types_.resize(size());

	has_energy_ = has_stresses_ = false;
}

Configuration::Configuration(	CrystalSystemType crystal_system, 
								double basic_lattice_parameter1, 
								double basic_lattice_parameter2, 
								double basic_lattice_parameter3, 
								int replication_times)
	: Configuration(crystal_system, 1, replication_times)
{
	Matrix3 stretch(	basic_lattice_parameter1, 0, 0,
						0, basic_lattice_parameter2, 0,
						0, 0, basic_lattice_parameter3);
	deform(stretch);
}

Configuration::~Configuration()
{
//	destroy();
}

bool Configuration::Load(std::ifstream& ifs)
{
	if (!ifs.is_open()) INPUT_ERROR("Input stream is not open");
	if (ifs.fail()) INPUT_ERROR("Input stream is open but not good");
	if (ifs.eof()) return EMPTY;

	char tag = 0;

	streampos cfg_beg_pos = ifs.tellg();
	if (ifs.fail()) ERROR("tellg failed");
	ifs >> tag;
	if (ifs.eof()) return EMPTY;
	if (ifs.fail()) ERROR("cannot read even one symbol!");
	ifs.seekg(cfg_beg_pos);
	if (ifs.fail()) ERROR("seekg failed");

	// checks if this is the binary file format
	if (tag == 'n')
		return LoadBin(ifs);

	string tmp_str;

	ifs >> tmp_str;

	// checks if this is the old file format
	if (tmp_str == "BEGIN") {
		ifs.seekg(cfg_beg_pos);
		if (ifs.fail()) ERROR("seekg failed");
		return LoadOld(ifs);
	}

	// checks if this is the new file format
	if (tmp_str != "BEGIN_CFG") ERROR("unknown token " + tmp_str);

	destroy();

	has_energy_ = has_stresses_ = false;

	// these are internal variables, if they remain false then this is an error
	bool _has_lattice = false;
	bool _has_atom_pos = false;
	int _size = -1;
	bool virial_str = false;

	while (!ifs.eof()) {
		ifs >> tmp_str;

		if (!ifs.good() && tmp_str != "END_CFG")
			INPUT_ERROR("Unable to read field name");

		if (tmp_str == "Size") {
			int new_size;
			ifs >> new_size;
			if (_size != -1 && _size != new_size)
				INPUT_ERROR("Size given, but does not match earlier size");
			_size = new_size;
			if (!ifs.good()) INPUT_ERROR("Unable to read configuration size");
			if (_size < 0)	INPUT_ERROR("Incorrect configuration size");
			resize(_size);
		}
		else if (tmp_str == "Supercell") {
			ifs >> lattice[0][0] >> lattice[0][1] >> lattice[0][2]
				>> lattice[1][0] >> lattice[1][1] >> lattice[1][2]
				>> lattice[2][0] >> lattice[2][1] >> lattice[2][2];
			if (!ifs.good()) INPUT_ERROR("Unable to read supercell");
			_has_lattice = true;
		} else if (tmp_str == "SuperCell") {
			ifs >> lattice[0][0] >> lattice[0][1] >> lattice[0][2]
				>> lattice[1][0] >> lattice[1][1] >> lattice[1][2]
				>> lattice[2][0] >> lattice[2][1] >> lattice[2][2];
			if (!ifs.good()) INPUT_ERROR("Unable to read supercell");
			_has_lattice = true;
		} else if (tmp_str == "AtomData:" || tmp_str == "Atomic_data:") {
			if (_size == 0)
				Warning("Configuration with no atoms has been read");

			if (_size == -1)
				INPUT_ERROR("Reading AtomData without Size given is not implemented.");

			enum class DataType { ID, TYPE, X, Y, Z, FX, FY, FZ, SITE_EN, CHARGE };
			vector<DataType> datamap;

			int control_summ_pos[3] = { 0,0,0 };
			int control_summ_frc[3] = { 0,0,0 };

			// in ids, '.first' is the id that is read and '.second' is the line number
			vector<int> ids;

			getline(ifs, tmp_str);
			istringstream keywords(tmp_str);
			do {
				string keyword;
				keywords >> keyword;

				if (keyword == "id") {
					ids.resize(_size);
					datamap.push_back(DataType::ID);
				} else if (keyword == "type") {
					types_.resize(_size);
					datamap.push_back(DataType::TYPE);
				} else if (keyword == "cartes_x") {
					pos_.resize(_size);
					datamap.push_back(DataType::X);
					control_summ_pos[0]++;
				} else if (keyword == "cartes_y") {
					pos_.resize(_size);
					datamap.push_back(DataType::Y);
					control_summ_pos[1]++;
				} else if (keyword == "cartes_z") {
					pos_.resize(_size);
					datamap.push_back(DataType::Z);
					control_summ_pos[2]++;
				} else if (keyword == "site_en") {
					has_site_energies(true);
					datamap.push_back(DataType::SITE_EN);
				} else if (keyword == "fx") {
					has_forces(true);
					datamap.push_back(DataType::FX);
					control_summ_frc[0]++;
				} else if (keyword == "fy") {
					has_forces(true);
					datamap.push_back(DataType::FY);
					control_summ_frc[1]++;
				} else if (keyword == "fz") {
					has_forces(true);
					datamap.push_back(DataType::FZ);
					control_summ_frc[2]++;
				} else if (keyword == "charge") {
					has_charges(true);
					datamap.push_back(DataType::CHARGE);
				} else if (keyword == "")
					break;
				else
					INPUT_ERROR("Unknown AtomData field");
			} while (keywords);

			// control_summ_pos should either be all 1 or all 0
			if (control_summ_pos[0] != control_summ_pos[1]
				|| control_summ_pos[0] != control_summ_pos[2]
				|| control_summ_pos[0] >= 2)
				INPUT_ERROR("Configuration reading eror. Wrong atom position information");
			if (control_summ_frc[0] != control_summ_frc[1]
				|| control_summ_frc[0] != control_summ_frc[2]
				|| control_summ_frc[0] >= 2)
				INPUT_ERROR("Configuration reading eror. Wrong force information");

			_has_atom_pos = (control_summ_pos[0] == 1);

			if (_size > 0) {
				for (int i = 0; i < size(); i++)
					for (int j = 0; j < (int)datamap.size(); j++)
						if (datamap[j] == DataType::TYPE)
							ifs >> types_[i];
						else if (datamap[j] == DataType::ID)
							ifs >> ids[i];
						else if (datamap[j] == DataType::X)
							ifs >> pos_[i][0];
						else if (datamap[j] == DataType::Y)
							ifs >> pos_[i][1];
						else if (datamap[j] == DataType::Z)
							ifs >> pos_[i][2];
						else if (datamap[j] == DataType::FX)
							ifs >> forces_[i][0];
						else if (datamap[j] == DataType::FY)
							ifs >> forces_[i][1];
						else if (datamap[j] == DataType::FZ)
							ifs >> forces_[i][2];
						else if (datamap[j] == DataType::SITE_EN)
							ifs >> site_energies_[i];
						else if (datamap[j] == DataType::CHARGE)
							ifs >> charges_[i];
						if (!ifs.good())
							INPUT_ERROR("Unable to read AtomData. Wrong Size specified?");
			} else {
				// HERE GOES THE CODE OF READING ATOMDATA WITHOUT SIZE GIVEN
			}

			// if ids are given (i.e., their size != 0) then check that they are 1, 2, 3,...
			for (int i = 0; i < (int)ids.size(); i++)
				if (ids[i] != i + 1)
					INPUT_ERROR("Invalid file format: wrong order of id\'s");

			if (!_has_atom_pos) INPUT_ERROR("Invalid file format: no atom positions");

			// if types are not given, then set all to zero
			if (types_.size() == 0) {
				types_.resize(size());
				FillWithZero(types_);
			}
		} else if (tmp_str == "Energy") {
			ifs >> energy;
			if (!ifs.good())
				INPUT_ERROR("Configuration reading eror. Reading energy failed");
			has_energy_ = true;
		} else if (tmp_str == "Stress:") {
			vector<double*> datamap;

			getline(ifs, tmp_str);
			istringstream keywords(tmp_str);

			do {
				string keyword;
				keywords >> keyword;
				if (keyword == "xx")
					datamap.push_back(&stresses[0][0]);
				else if (keyword == "yy")
					datamap.push_back(&stresses[1][1]);
				else if (keyword == "zz")
					datamap.push_back(&stresses[2][2]);
				else if (keyword == "xy")
					datamap.push_back(&stresses[0][1]);
				else if (keyword == "xz")
					datamap.push_back(&stresses[0][2]);
				else if (keyword == "yz")
					datamap.push_back(&stresses[1][2]);
			} while (keywords);

			for (int j = 0; j < (int)datamap.size(); j++)
				ifs >> *datamap[j];
			if (!ifs.good())
				INPUT_ERROR("Configuration reading error. Failed to read stresses");

			stresses[1][0] = stresses[0][1];
			stresses[2][1] = stresses[1][2];
			stresses[2][0] = stresses[0][2];
			has_stresses_ = true;
		} else if (tmp_str == "Virial:") {
			virial_str = true;
			vector<double*> datamap;

			getline(ifs, tmp_str);
			istringstream keywords(tmp_str);

			do {
				string keyword;
				keywords >> keyword;
				if (keyword == "xx")
					datamap.push_back(&stresses[0][0]);
				else if (keyword == "yy")
					datamap.push_back(&stresses[1][1]);
				else if (keyword == "zz")
					datamap.push_back(&stresses[2][2]);
				else if (keyword == "xy")
					datamap.push_back(&stresses[0][1]);
				else if (keyword == "xz")
					datamap.push_back(&stresses[0][2]);
				else if (keyword == "yz")
					datamap.push_back(&stresses[1][2]);
			} while (keywords);

			for (int j = 0; j < (int)datamap.size(); j++)
				ifs >> *datamap[j];
			if (!ifs.good())
				INPUT_ERROR("Configuration reading error. Failed to read stresses");

			stresses[1][0] = stresses[0][1];
			stresses[2][1] = stresses[1][2];
			stresses[2][0] = stresses[0][2];
			has_stresses_ = true;
		} else if (tmp_str == "Features:") {
			getline(ifs, tmp_str);
			istringstream keywords(tmp_str);

			do {
				string keyword;
				keywords >> keyword;
				if (keyword != "")
					ifs >> features[keyword];
			} while (keywords);

			if (!ifs.good())
				INPUT_ERROR("Configuration reading error (Broken features)");
		} else if (tmp_str == "Feature") {
			string key, value;
			ifs >> key;
			ifs.ignore(1);
			key = mlip_string_unescape(key);
			getline(ifs, value);
			if (!ifs.good())
				INPUT_ERROR("Configuration reading error (Broken feature)");

			if (features.count(key) == 0)
				features[key] = value;
			else // multiline feature value
				features[key] += '\n' + value;
		} else if (tmp_str == "END_CFG")
			break;
		else
			INPUT_ERROR("Configuration reading error. Unknown field \"" + tmp_str + "\"");
	}

	if (!_has_lattice)
		INPUT_ERROR("configuration has no lattice");
	else if (virial_str)
		stresses *= 1.0 / fabs(lattice.det());
	if (!_has_atom_pos && size() == 0)
		return EMPTY;
	else
		return VALID;
}

bool Configuration::LoadOld(ifstream& ifs)
{
	destroy();

	if (!ifs.is_open())
		INPUT_ERROR("Input stream not open");

	has_energy_ = has_stresses_ = false;

	bool _has_lattice = false;
	bool _has_atom_pos = false;
	bool _has_atom_types = false;
	int _size = 0;

	string tmp_str = "";

	ifs >> tmp_str;
	if (tmp_str != "BEGIN")
		return EMPTY;

	while (!ifs.eof())
	{
		ifs >> tmp_str;
		if (ifs.fail()) INPUT_ERROR("Unable to read field name");

		if (tmp_str == "Size")
		{
			ifs >> _size;
		}
		else if (tmp_str == "MinusStress")
		{
			ifs >> stresses[0][0]
				>> stresses[1][1]
				>> stresses[2][2]
				>> stresses[0][1]
				>> stresses[1][2]
				>> stresses[0][2];

			stresses[1][0] = stresses[0][1];
			stresses[2][1] = stresses[1][2];
			stresses[2][0] = stresses[0][2];

			stresses = -stresses;
			has_stresses_ = true;
		}
		else if (tmp_str == "MinusStress9")
		{
			ifs >> stresses[0][0]
				>> stresses[0][1]
				>> stresses[0][2]
				>> stresses[1][0]
				>> stresses[1][1]
				>> stresses[1][2]
				>> stresses[2][0]
				>> stresses[2][1]
				>> stresses[2][2];

			stresses = -stresses;
			has_stresses_ = true;
		}
		else if ((tmp_str == "VirialStress") || (tmp_str == "Stress"))
		{
			ifs >> stresses[0][0]
				>> stresses[1][1]
				>> stresses[2][2]
				>> stresses[0][1]
				>> stresses[1][2]
				>> stresses[0][2];

			stresses[1][0] = stresses[0][1];
			stresses[2][1] = stresses[1][2];
			stresses[2][0] = stresses[0][2];
			has_stresses_ = true;
		}
		else if (tmp_str == "Lattice")
		{
			ifs >> lattice[0][0] >> lattice[0][1] >> lattice[0][2]
				>> lattice[1][0] >> lattice[1][1] >> lattice[1][2]
				>> lattice[2][0] >> lattice[2][1] >> lattice[2][2];
			_has_lattice = true;
		}
		else if ((tmp_str == "AtmData") || (tmp_str == "AtomData"))
		{
			double x, y, z;
			// loop while we keep reading numbers
			for (ifs >> x; ifs.good(); ifs >> x) {
				ifs >> y >> z;
				if (ifs.good()) {
					pos_.emplace_back();
					pos_.back()[0] = x;
					pos_.back()[1] = y;
					pos_.back()[2] = z;
				}
				else INPUT_ERROR("Error reading coordinates");

				ifs >> x >> y >> z;
				if (ifs.good()) {
					forces_.emplace_back();
					forces_.back()[0] = x;
					forces_.back()[1] = y;
					forces_.back()[2] = z;
				}
				else INPUT_ERROR("Error reading forces");
			}
			ifs.clear();
			_has_atom_pos = true;
		}
		else if (tmp_str == "AtomPos")
		{
			double x, y, z;
			// loop while we keep reading numbers
			for (ifs >> x; ifs.good(); ifs >> x) {
				ifs >> y >> z;
				if (ifs.good()) {
					pos_.emplace_back();
					pos_.back()[0] = x;
					pos_.back()[1] = y;
					pos_.back()[2] = z;
				}
				else INPUT_ERROR("Error reading coordinates");
			}
			ifs.clear();
			_has_atom_pos = true;
		}
		else if (tmp_str == "AtomTypes") 
		{
			int type;
			// loop while we keep reading numbers
			for (ifs >> type; ifs.good(); ifs >> type)
				types_.push_back(type);
			ifs.clear();
			_has_atom_types = true;
		} 
		else if (tmp_str == "SiteEnergies")
		{
			double foo;
			// loop while we keep reading numbers
			ifs >> foo;
			while (ifs.good())
			{
				site_energies_.push_back(foo);
				ifs >> foo;
			}
			ifs.clear();
		}
		else if (tmp_str == "AtomForces")
		{
			double x, y, z;
			// loop while we keep reading numbers
			for (ifs >> x; ifs.good(); ifs >> x) {
				ifs >> y >> z;
				if (ifs.good()) {
					forces_.emplace_back();
					forces_.back()[0] = x;
					forces_.back()[1] = y;
					forces_.back()[2] = z;
				}
				else INPUT_ERROR("Error reading forces");
			}
			ifs.clear();
		}
		else if (tmp_str == "Charges")
		{
			double foo;
			// loop while we keep reading numbers
			ifs >> foo;
			while (!ifs.fail())
			{
				charges_.push_back(foo);
				ifs >> foo;
			}
			ifs.clear();
		}
		else if (tmp_str == "Energy")
		{
			ifs >> energy;
			if (!ifs.good()) INPUT_ERROR("Error reading energy");
			has_energy_ = true;
		}
		else if (tmp_str == "Feature")
		{
			std::string name, value;
			ifs >> name >> value;
			features[name] = value;
		}
		else if (tmp_str == "END")
			break;
		else 
			INPUT_ERROR("Unknown configuration field \"" + tmp_str + "\"");

		if (!ifs.good()) INPUT_ERROR("error reading");
	}
	
	if (!(_has_atom_pos && _has_lattice)) INPUT_ERROR("No lattice or atom positions");
	if (_size == 0)
		_size = (int)pos_.size();
	if (_has_atom_types && types_.size() != _size) INPUT_ERROR("Problem with configuration size");
	if (_size != (int)pos_.size()) INPUT_ERROR("Problem with configuration size");
	if (has_forces() && (_size != (int)forces_.size())) INPUT_ERROR("Problem with configuration size");
	if (has_site_energies() && (_size != (int)site_energies_.size())) INPUT_ERROR("Problem with configuration size");
	if (has_charges() && (_size != (int)charges_.size())) INPUT_ERROR("Problem with configuration size");

	if (!_has_atom_types) types_.resize(_size, 0);
	_has_atom_types = true;

	return VALID;
}

bool Configuration::LoadBin(std::ifstream & ifs)
{
	destroy();

	if (!ifs.is_open())
		INPUT_ERROR("Input stream not open");

	char tag = 0;
	has_energy_ = has_stresses_ = false;
	bool has_pos = false;
	bool has_lattice = false;
	bool has_types = false;
	int _size;

	ifs >> tag;
	if (tag != 'n') {
		if (ifs.eof()) return false; // empty configuration
		INPUT_ERROR("Corrupted binary configuration file");
	}
	else
		ifs.read((char*) &_size, sizeof(_size));

	if (_size < 0)
		INPUT_ERROR("Corrupted binary configuration file");

	while (!ifs.eof())
	{
		ifs >> tag;

		if (tag == 'l') {
			for (int a = 0; a < 3; a++)
				for (int b = 0; b < 3; b++)
					ifs.read((char*)&lattice[a][b], sizeof(lattice[0][0]));
			has_lattice = true;
		}
		else if (tag == 'p') {
			pos_.resize(_size);
			ifs.read((char*)&pos_[0], _size*sizeof(pos_[0]));
			has_pos = true;
		}
		else if (tag == 't') {
			types_.resize(_size);
			ifs.read((char*)&types_[0], _size*sizeof(types_[0]));
			has_types = true;
		} 
		else if (tag == 'f') {
			forces_.resize(_size);
			ifs.read((char*)&forces_[0], _size*sizeof(forces_[0]));
		} 
		else if (tag == 's') {
			ifs.read((char*)&stresses[0][0], sizeof(stresses[0][0]));
			ifs.read((char*)&stresses[1][1], sizeof(stresses[0][0]));
			ifs.read((char*)&stresses[2][2], sizeof(stresses[0][0]));
			ifs.read((char*)&stresses[0][1], sizeof(stresses[0][0]));
			ifs.read((char*)&stresses[1][2], sizeof(stresses[0][0]));
			ifs.read((char*)&stresses[0][2], sizeof(stresses[0][0]));
			stresses[1][0] = stresses[0][1];
			stresses[2][1] = stresses[1][2];
			stresses[2][0] = stresses[0][2];
			has_stresses_ = true;
		}
		else if (tag == 'v') {
			site_energies_.resize(_size);
			ifs.read((char*)&site_energies_[0], _size*sizeof(site_energies_[0]));
		}
		else if (tag == 'c') {
			charges_.resize(_size);
			ifs.read((char*)&charges_[0], _size*sizeof(charges_[0]));
		}
		else if (tag == 'e') {
			ifs.read((char*)&energy, sizeof(energy));
			has_energy_ = true;
		} 
		else if (tag == 'F') {
			std::string name, value;
			int l;
			ifs.read((char*)&l, sizeof(l));
			name.resize(l);
			ifs.read(&name[0], l * sizeof(char));
			ifs.read((char*)&l, sizeof(l));
			value.resize(l);
			ifs.read(&value[0], l * sizeof(char));
			features[name] = value;
		} 
		else if (tag == '#')
			break;
		else
			INPUT_ERROR("Unknown tag \'" + tag + "\'");
	}

	if (!has_types) types_.resize(_size, 0);
	has_types = true;

	if (!(has_lattice && has_pos)) INPUT_ERROR("Corrupted binary configuration file");

	return VALID;
}

// Configuration file format definitions
#define LAT_FORMATTING right << setw(LAT_WIDTH) << setprecision(LAT_PRECISION)
#define IND_FORMATTING right << setw(IND_WIDTH) << setprecision(IND_PRECISION)
#define TYP_FORMATTING right << setw(TYP_WIDTH) << setprecision(TYP_PRECISION)
#define POS_FORMATTING right << setw(POS_WIDTH) << setprecision(POS_PRECISION)
#define SEN_FORMATTING right << setw(SEN_WIDTH) << setprecision(SEN_PRECISION)
#define FRC_FORMATTING right << setw(FRC_WIDTH) << setprecision(FRC_PRECISION)
#define CHG_FORMATTING right << setw(CHG_WIDTH) << setprecision(CHG_PRECISION)
#define ENE_FORMATTING right << setw(ENE_WIDTH) << setprecision(ENE_PRECISION)
#define STR_FORMATTING right << setw(STR_WIDTH) << setprecision(STR_PRECISION)

void Configuration::Save(ofstream& ofs, unsigned int flags) const
{
	int LAT_WIDTH; int LAT_PRECISION;
	int IND_WIDTH; int IND_PRECISION;
	int TYP_WIDTH; int TYP_PRECISION;
	int POS_WIDTH; int POS_PRECISION;
	int SEN_WIDTH; int SEN_PRECISION;
	int FRC_WIDTH; int FRC_PRECISION;
	int CHG_WIDTH; int CHG_PRECISION;
	int ENE_WIDTH; int ENE_PRECISION;
	int STR_WIDTH; int STR_PRECISION;

	unsigned int save_binary = flags & SAVE_BINARY;
	unsigned int without_loss = flags & SAVE_NO_LOSS;
	unsigned int direct_atom_coord = flags & SAVE_DIRECT_COORDS;

	if (!ofs.is_open())
		INPUT_ERROR("Output stream not open");

	if (save_binary) {
		SaveBin(ofs);
		return;
	}

	ofs.precision(16);
	if (without_loss) {
		ofs << scientific;
		LAT_WIDTH = 1; LAT_PRECISION = 16;
		IND_WIDTH = 1; IND_PRECISION = 0;
		TYP_WIDTH = 1; TYP_PRECISION = 0;
		POS_WIDTH = 1; POS_PRECISION = 16;
		SEN_WIDTH = 1; SEN_PRECISION = 16;
		FRC_WIDTH = 1; FRC_PRECISION = 16;
		CHG_WIDTH = 1; CHG_PRECISION = 16;
		ENE_WIDTH = 1; ENE_PRECISION = 16;
		STR_WIDTH = 1; STR_PRECISION = 16;
	}
	else {
		ofs << fixed;
		LAT_WIDTH = 13; LAT_PRECISION =  6;
		IND_WIDTH = 10; IND_PRECISION =  0;
		TYP_WIDTH =  4; TYP_PRECISION =  0;
		POS_WIDTH = 13; POS_PRECISION =  6;
		SEN_WIDTH = 17; SEN_PRECISION = 12;
		FRC_WIDTH = 11; FRC_PRECISION =  6;
		CHG_WIDTH =  8; CHG_PRECISION =  4;
		ENE_WIDTH = 20; ENE_PRECISION = 12;
		STR_WIDTH = 11; STR_PRECISION =  5;
	}

	int _size = size();

	ofs << "BEGIN_CFG\n";

	ofs << " Size\n    " << _size << '\n';

	ofs << " Supercell\n";
	for (int i = 0; i < 3; i++) 
		ofs << "    " << LAT_FORMATTING << lattice[i][0]
			<< ' ' << LAT_FORMATTING << lattice[i][1]
			<< ' ' << LAT_FORMATTING << lattice[i][2] << '\n';

	{
		ofs << " AtomData:";
		ofs << "  id";
		ofs << " type";
		
			if (direct_atom_coord)
				ofs << "       direct_x      direct_y      direct_z";
			else	
				ofs << "       cartes_x      cartes_y      cartes_z";
		if (has_site_energies())	ofs << "    site_en";
		if (has_forces())			ofs << "           fx          fy          fz";
		if (has_charges())			ofs << "      charge";
		ofs << '\n';

		for (int i=0; i<size(); i++)
		{
			ofs << "    " << IND_FORMATTING << i+1;
			ofs << ' ' << TYP_FORMATTING << types_[i];
				if (!direct_atom_coord)
					ofs << "  " << POS_FORMATTING << pos_[i][0]
						<< ' ' << POS_FORMATTING << pos_[i][1]
						<< ' ' << POS_FORMATTING << pos_[i][2];
				else
				{
					Vector3 dpos = direct_pos(i);
					ofs << "  " << POS_FORMATTING << dpos[0]
						<< ' ' << POS_FORMATTING << dpos[1]
						<< ' ' << POS_FORMATTING << dpos[2];
				}
			if (has_site_energies())	
				ofs << "  " << SEN_FORMATTING << site_energy(i);
			if (has_forces())	
				ofs << "  " << FRC_FORMATTING << forces_[i][0]
					<< ' ' << FRC_FORMATTING << forces_[i][1]
					<< ' ' << FRC_FORMATTING << forces_[i][2];
			if (has_charges())	
				ofs << "    " << CHG_FORMATTING << charges(i);
			ofs << '\n';
		}
	}

	if (has_energy()) 
		ofs << " Energy\n    " << ENE_FORMATTING << energy << '\n';

	if (has_stresses())
	{
		ofs << " Stress:   xx          yy          zz"
			<< "          yz          xz          xy\n";

			ofs << ' ' << STR_FORMATTING << stresses[0][0]
				<< ' ' << STR_FORMATTING << stresses[1][1]
				<< ' ' << STR_FORMATTING << stresses[2][2];
			ofs << ' ' << STR_FORMATTING << stresses[1][2]
				<< ' ' << STR_FORMATTING << stresses[0][2]
				<< ' ' << STR_FORMATTING << stresses[0][1];
		ofs << '\n';
	}

	if (features.size()>0)
	{
		for (auto& feature : features) 
		{
			std::string key = std::string(feature.first);
			std::string val = std::string(feature.second);
			while (!val.empty() && !key.empty())
			{
				size_t cut = val.find_first_of('\n');
				if (cut > val.size())
				{
					ofs << " Feature   " << mlip_string_escape(key) << '\t' << val << '\n';
					break;
				}
				else
				{
					ofs << " Feature   " << mlip_string_escape(key)
						<< '\t' << val.substr(0, cut) << '\n';
					val = val.substr(cut+1);
					if (val.empty())
						ofs << " Feature   " << mlip_string_escape(key)
							<< "\t\n";
				}
			}
		}
	}

	ofs << "END_CFG\n" << endl;
}

void Configuration::AppendToFile(const std::string & filename, unsigned int flags) const
{
	ofstream ofs(filename, ios::app);
	Save(ofs, flags);
}

void Configuration::SaveBin(std::ofstream & ofs) const
{
	if (!ofs.is_open())
		INPUT_ERROR("Output stream not open");

	int _size = size();

	ofs << 'n';
	ofs.write((char*) &_size, sizeof(_size));

	ofs << 'l';
	for (int a = 0; a < 3; a++)
		for (int b = 0; b < 3; b++)
			ofs.write((char*) &lattice[a][b], sizeof(double));

	ofs << 'p';
	if ((_size > 0) && (!pos_.empty()))
		ofs.write((char*)&pos_[0], _size*sizeof(pos_[0]));

	ofs << 't';
	if ((_size > 0) && (!types_.empty()))
		ofs.write((char*)&types_[0], _size*sizeof(types_[0]));

	if (has_forces())
	{
		ofs << 'f';
		ofs.write((char*)&forces_[0], _size*sizeof(forces_[0]));
	}

	if (has_site_energies())
	{
		ofs << 'v';
		ofs.write((char*)&site_energies_[0], _size*sizeof(site_energies_[0]));
	}

	if (has_charges())
	{
		ofs << 'c';
		ofs.write((char*)&charges_[0], _size*sizeof(charges_[0]));
	}

	if (has_stresses())
	{
		ofs << 's';
		ofs.write((char*)&stresses[0][0], sizeof(stresses[0][0]));
		ofs.write((char*)&stresses[1][1], sizeof(stresses[0][0]));
		ofs.write((char*)&stresses[2][2], sizeof(stresses[0][0]));
		ofs.write((char*)&stresses[0][1], sizeof(stresses[0][0]));
		ofs.write((char*)&stresses[1][2], sizeof(stresses[0][0]));
		ofs.write((char*)&stresses[0][2], sizeof(stresses[0][0]));
	}

	if (has_energy())
	{
		ofs << 'e';
		ofs.write((char*)&energy, sizeof(energy));
	}
	
	for (auto& feature : features) 
	{
		ofs << 'F';
		int l;
		l = (int)feature.first.length();
		ofs.write((char*)&l, sizeof(l));
		ofs.write((char*)&feature.first[0], l*sizeof(char));
		l = (int)feature.second.length();
		ofs.write((char*)&l, sizeof(l));
		ofs.write((char*)&feature.second[0], l*sizeof(char));
	}

	ofs << '#';
	ofs.flush();
}

bool Configuration::ReadEFSFromProfessOutput(const std::string&  filename)
{
	std::ifstream ifs(filename);
	if (!ifs.is_open())
		INPUT_ERROR("Can't open PROFESS output file \"" + filename + "\" for input");

	has_energy_ = has_stresses_ = false;

	int _size = size();

	string tmp_str = "";
	char tmp_buf[1000] = "";

	while (strncmp(tmp_buf, "#   TOTAL POTENTIAL ENERGY = ", 28))
	{
		ifs.getline(tmp_buf, 1000);
		if (ifs.eof())
			INPUT_ERROR("Can't find energy in PROFESS output file\n");
	}
	for (int i = 0; i < 4; i++)
		ifs >> tmp_str;
	ifs >> energy;
	if (ifs.fail())
		INPUT_ERROR("Error reading energy");
	else
		has_energy_ = true;

	for (int i = 0; i < 7; i++)
		ifs.getline(tmp_buf, 1000);
	forces_.resize(size());
	for (int i = 0; i < _size; i++)
	{
		ifs.ignore(12);
		ifs >> forces_[i][0] >> forces_[i][1] >> forces_[i][2];
		ifs.ignore(100, '\n');
	}
	if (ifs.fail() || ifs.eof())
		INPUT_ERROR("Error reading");

	while (strncmp(tmp_buf, "                                STRESS (GPa)", 44))
	{
		ifs.getline(tmp_buf, 1000);
		if (ifs.eof())
			INPUT_ERROR("Can't find energy in PROFESS output file\n");
	}
	ifs.ignore(15);
	ifs >> stresses[0][0] >> stresses[0][1] >> stresses[0][2];
	ifs.ignore(15);
	ifs >> stresses[1][0] >> stresses[1][1] >> stresses[1][2];
	ifs.ignore(15);
	ifs >> stresses[2][0] >> stresses[2][1] >> stresses[2][2];
	stresses[0][1] = stresses[1][0] = (stresses[0][1] + stresses[1][0]) / 2;
	stresses[0][2] = stresses[2][0] = (stresses[0][2] + stresses[2][0]) / 2;
	stresses[2][1] = stresses[1][2] = (stresses[2][1] + stresses[1][2]) / 2;

	const double eVA3_to_GPa = 160.2176487;
	stresses *= lattice.det() / eVA3_to_GPa;
	
	if (ifs.fail() || ifs.eof())
		INPUT_ERROR("Error reading energy");
	else
		has_stresses_ = true;

	ifs.close();

	return VALID;
}

void Configuration::WriteProfessIon(const std::string&  filename, std::string* species_table_tag) const
{
	std::ofstream ofs(filename);
	if (!ofs.is_open())
		INPUT_ERROR("Can't open PROFESS input file \"" + filename + "\" for writing");

	int _size = size();

	ofs << "%BLOCK LATTICE_CART\n";
	ofs << '\t' << lattice[0][0] << '\t' << lattice[0][1] << '\t' << lattice[0][2] << '\n'
		<< '\t' << lattice[1][0] << '\t' << lattice[1][1] << '\t' << lattice[1][2] << '\n'
		<< '\t' << lattice[2][0] << '\t' << lattice[2][1] << '\t' << lattice[2][2] << '\n';
	ofs << "%END BLOCK LATTICE_CART\n";
	ofs << "%BLOCK POSITIONS_CART\n";
	for (int i = 0; i < _size; i++)
		ofs << '\t' << species_table_tag[types_[i]] << '\t' << pos_[i][0] << '\t' << pos_[i][1] << '\t' << pos_[i][2] << '\n';
	ofs << "%END BLOCK POSITIONS_CART\n";
	ofs << "%BLOCK SPECIES_POT\n";
	set<int> atom_types;
	for (int i = 0; i < _size; i++)
		atom_types.insert(types_[i]);
	for (auto p_type = atom_types.begin(); p_type != atom_types.end(); ++p_type)
		ofs << '\t' << species_table_tag[*p_type] << ' ' << species_table_tag[*p_type] << "_HC.lda.recpot\n";
	ofs << "%END BLOCK SPECIES_POT\n";

	ofs.close();
}

bool Configuration::LoadNextFromOUTCAR(std::ifstream& ifs,int maxiter)
{
	std::string line;


	int iter = -1;
	

	// Skipping to the end of the SCF loop
	while ((line.substr(0, 104) !=
		"------------------------ aborting loop because EDIFF is reached ----------------------------------------")
		&&
		(line.substr(0, 43) != "                  Total CPU time used (sec)")
		) {
		std::getline(ifs, line);

		if (ifs.eof())
			INPUT_ERROR((string)"Outcar-file parsing error: Can't find string \"aborting loop because EDIFF is reached\""); // remove: 3
		//Reading number of ionic iterations
		else if (line.substr(0, 51) == "----------------------------------------- Iteration")
		{
			line.erase(0, 51);

			std::stringstream stream(line);

			string s = "";
			char t=(char)0;

			while (t != '(')
				t = (char)stream.get();

			stream >> iter;
			
		}
	}

	if (maxiter != 0 && iter >= maxiter) {
		features["EFS_by"] = "VASP_not_converged";
		Warning("Loading configuration from OUTCAR: non-converged calculation detected. " + 
				 std::to_string(iter) + " iterations done");
	}
	else
		features["EFS_by"] = "VASP";

	// No "Total CPU time used (sec)" = likely broken outcar
	if (line.substr(0, 43) == "                  Total CPU time used (sec)")
		return EMPTY;

	// We now expect a configuration, so we can erase the current one
	has_energy(false);
	has_forces(false);
	has_stresses(false);
	pos_.clear();
	forces_.clear();

	// stresses reading block
	{
		// read until we find stresses (sometimes absent in the file) or lattice (always present)
		while (
			(line.substr(0, 24) != "  FORCE on cell =-STRESS") &&
			(line.substr(0, 71) != "      direct lattice vectors                 reciprocal lattice vectors")
			) {
			std::getline(ifs, line);
			if (ifs.eof())
				INPUT_ERROR((string)"Outcar-file parsing error: Can't find stresses");
		}

		// check if found the stresses
		if (line.substr(0, 24) == "  FORCE on cell =-STRESS") {

			while (line.substr(0, 7) != "  Total") {
				std::getline(ifs, line);
				if (ifs.eof())
					INPUT_ERROR((string)"Outcar-file parsing error: Can't find the stresses\"");
			}

			// now parsing the line
			std::stringstream stream(line);

			has_stresses(true);
			std::string tempstring;
			stream >> tempstring;

			stream >> stresses[0][0]
				>> stresses[1][1]
				>> stresses[2][2]
				>> stresses[0][1]
				>> stresses[1][2]
				>> stresses[0][2];

			stresses *= -1;

			stresses[1][0] = stresses[0][1];
			stresses[2][1] = stresses[1][2];
			stresses[2][0] = stresses[0][2];
		}
	}

	{// lattice reading block
		while (line.substr(0, 71) != "      direct lattice vectors                 reciprocal lattice vectors") {
			std::getline(ifs, line);
			if (ifs.eof())
				INPUT_ERROR((string)"Outcar-file parsing error: Can't find lattice data");
		}
		double foo;
		ifs >> lattice[0][0] >> lattice[0][1] >> lattice[0][2] >> foo >> foo >> foo;
		ifs >> lattice[1][0] >> lattice[1][1] >> lattice[1][2] >> foo >> foo >> foo;
		ifs >> lattice[2][0] >> lattice[2][1] >> lattice[2][2] >> foo >> foo >> foo;
	}

	{// positions and forces reading block
									   
		while (line.substr(0, 70) != " POSITION                                       TOTAL-FORCE (eV/Angst)") {
			std::getline(ifs, line);

			if (ifs.eof())
				INPUT_ERROR((string)"Outcar-file parsing error: Can't find forces");
				

		}

		std::getline(ifs, line);
		while ((!ifs.fail()) && (!ifs.eof())) {
			pos_.emplace_back();
			forces_.emplace_back();
			ifs >> pos_.back()[0] >> pos_.back()[1] >> pos_.back()[2]
				>> forces_.back()[0] >> forces_.back()[1] >> forces_.back()[2];
		}

		if (ifs.eof())
			INPUT_ERROR("error reading");
		ifs.clear();

		pos_.pop_back();
		forces_.pop_back();
	}

	{// energy reading block
		while (line.substr(0, 46) != "  FREE ENERGIE OF THE ION-ELECTRON SYSTEM (eV)") {
			std::getline(ifs, line);
			if (ifs.eof())
				INPUT_ERROR((string)"Outcar-file parsing error: Can't find energy");
		}
		std::getline(ifs, line);
		string str1 = "";
		string str2 = "";
		string str3 = "";
		string str4 = "";
		ifs >> str1 >> str2 >> str3 >> str4;
		if (str1 != "free" && str2 != "energy" && str3 != "TOTEN" && str4 != "=")
			INPUT_ERROR((string)"Outcar-file parsing error: Can't read energy");
		ifs >> energy;
	}

	if (ifs.fail() || ifs.eof()) {
		ifs.close();
		INPUT_ERROR("error reading");
	} else {
		has_energy_ = true;
		site_energies_.clear();
	}

	return VALID;
}


int LoadPreambleFromOUTCAR(std::ifstream& ifs, std::vector<int>& ions_per_type)
{
	std::string line;

	// Finding ions per type data by keywords
	while (line.substr(0, 18) !=
		"   ions per type =") {
		std::getline(ifs, line);
		if (ifs.eof())
			INPUT_ERROR((string)"\"ions per type\" not given");
	}

	// now parsing the line, extracing ions per type
	{
		line.erase(0, 18);
		std::stringstream stream(line);
		int num;
		stream >> num;
		while (stream) {
			ions_per_type.emplace_back(num);
			stream >> num;
		}
	}


	//Reading max. number of ionic iterations
	while (line.substr(0, 11) !=
		"   NELM   =") {
		std::getline(ifs, line);
		if (ifs.eof())
			INPUT_ERROR((string)"\"Number of electronic iterations\" not given");
	}

	{
		line.erase(0, 11);
		std::stringstream stream(line);
		int maxiter = -1;
		stream >> maxiter;
		return maxiter;

	}



}

void Configuration::LoadFromOUTCAR(const std::string& filename)
{
	std::ifstream ifs(filename);
	if (!ifs.is_open())
		INPUT_ERROR((string)"Can't open OUTCAR file \"" + filename + "\" for reading");

	std::vector<int> ions_per_type;
	int maxiter = LoadPreambleFromOUTCAR(ifs, ions_per_type);

	if (!LoadNextFromOUTCAR(ifs, maxiter))
		ERROR("No configuration read from OUTCAR");

	std::string line;
	while (line.substr(0, 36) != " total amount of memory used by VASP") {
		std::getline(ifs, line);
		if (ifs.eof())
			INPUT_ERROR((string)"OUTCAR does not end with \"total amount of memory used by VASP\"");
	}

	// filling types from ions_per_type
	types_.resize(size(), 0);
	int i = 0;
	for (int k = 0; k < (int)ions_per_type.size(); k++)
		for (int j = 0; j < ions_per_type[k]; j++)
			types_[i++] = k;
	if (i != size())
		INPUT_ERROR((string)"Outcar-file parsing error: ions_per_type do not match the atoms count");
}

//! Assumes atom types 0, 1, ... correspond to those in POTCAR.
void Configuration::WriteVaspPOSCAR(const std::string& filename) const
{
	const int LAT_WIDTH = 13; const int LAT_PRECISION = 6;
	const int POS_WIDTH = 13; const int POS_PRECISION = 6;

	std::ofstream ofs(filename);
	if (!ofs.is_open())
		INPUT_ERROR((string)"Can't open file \"" + filename + "\" for output");

	ofs.precision(16);

	ofs << "MLIP output to VASP\n";

	int _size = size();

	// calculating the maximal atom type (minimal is 0)
	int max_type = 0;
	for (int i = 0; i < _size; i++)
		if (max_type < type(i)) max_type = type(i);

	// calculating of number of atoms per type
	std::vector<int> type_count;
	type_count.resize(1+max_type);
	type_count.assign(1+max_type, 0);
	for (int i = 0; i < _size; i++)
		type_count[type(i)] ++;

	double scale_factor = 1.0;	

	ofs << scale_factor << '\n';
	for	(int i = 0; i < 3; i++)
		ofs << "    " << LAT_FORMATTING << lattice[i][0] / scale_factor
			<< ' ' << LAT_FORMATTING << lattice[i][1] / scale_factor
			<< ' ' << LAT_FORMATTING << lattice[i][2] / scale_factor << '\n';

	for (int t = 0; t <= max_type; t++)
		ofs << ' ' << type_count[t];
	ofs << '\n';
	ofs << "cart\n";
	for (int t = 0; t <= max_type; t++)
		for (int i = 0; i < _size; i++)
			if(type(i) == t)
				ofs << "    " << POS_FORMATTING << pos_[i][0] / scale_factor
					<< ' ' << POS_FORMATTING << pos_[i][1] / scale_factor
					<< ' ' << POS_FORMATTING << pos_[i][2] / scale_factor << '\n';

	ofs.close();
}

bool LoadDynamicsFromOUTCAR(std::vector<Configuration> &db, const std::string& filename)
{
	std::ifstream ifs(filename);
	if (!ifs.is_open())
		INPUT_ERROR((string)"Can't open OUTCAR file \"" + filename + "\" for reading");

	std::string line;
	std::vector<int> ions_per_type;

	LoadPreambleFromOUTCAR(ifs, ions_per_type);
	MlipInputException err("");
	bool caught = false;
	bool status = false;
	for (Configuration cfg; ; db.push_back(cfg)) {
		try { status = cfg.LoadNextFromOUTCAR(ifs); }
		catch (const MlipInputException &e) {
			err = e; caught = true;
		}
		if (caught || !status) break;
		// filling types from ions_per_type
		cfg.types_.resize(cfg.size(), 0);
		int i = 0;
		for (int k = 0; k < (int)ions_per_type.size(); k++)
			for (int j = 0; j < ions_per_type[k]; j++)
				cfg.types_[i++] = k;
		if (i != cfg.size())
			INPUT_ERROR((string)"OUTCAR file parsing error: ions_per_type (" + std::to_string(i) + ") do not match the atoms count (" + std::to_string(cfg.size()) + ")");
	}

	return !caught;
}

//! loads the last configuration from OUTCAR dynamics
//! returns false if the OUTCAR is broken, but at least one configuration was read successfully
bool Configuration::LoadLastFromOUTCAR(const std::string& filename)
{
	std::ifstream ifs(filename);
	if (!ifs.is_open())
		INPUT_ERROR((string)"Can't open OUTCAR file \"" + filename + "\" for reading");

	std::string line;
	std::vector<int> ions_per_type;

	LoadPreambleFromOUTCAR(ifs, ions_per_type);

	MlipInputException err("");
	int i = 0;
	bool caught = false;
	try 
	{
		while (LoadNextFromOUTCAR(ifs))
			;
		// filling types from ions_per_type
		types_.resize(size(), 0);
		for (int k = 0; k < (int)ions_per_type.size(); k++)
			for (int j = 0; j < ions_per_type[k]; j++)
				types_[i++] = k;
	}
	catch (const MlipInputException &e) {
		err = e; caught = true;
	}

	if (i != size()) {
		if(caught)
			INPUT_ERROR((std::string)err.What() + "\nAlso: tried to return the latest good config,\n  but ions_per_type (" + std::to_string(i) + ") do not match the atoms count (" + std::to_string(size()) + ")\\\n");
		INPUT_ERROR((string)"OUTCAR file parsing error: ions_per_type (" + std::to_string(i) + ") do not match the atoms count (" + std::to_string(size()) + ")");
	}
	return !caught;
}

//! stupid O(N^2) algorithm
double Configuration::MinDist()
{
	double mindist = 9.9e99;
	for (int ind2 = 0; ind2 < size(); ind2++)
		for (int ind = ind2; ind < size(); ind++)
			for (int i = -1; i <= 1; i++)
				for (int j = -1; j <= 1; j++)
					for (int k = -1; k <= 1; k++)
						if ((ind2 != ind) || (i != 0) || (j != 0) || (k != 0)) {
							Vector3 candidate = pos(ind);
							for (int a = 0; a < 3; a++)
								candidate[a] += i*lattice[0][a] + j*lattice[1][a] + k*lattice[2][a];
							mindist = __min(mindist, distance(pos(ind2), candidate));
						}
	return mindist;
}

vector<Configuration> LoadCfgs(const string& filename)
{
	vector<Configuration> cfgs;
	ifstream ifs(filename, ios::binary);
	if (!ifs.is_open())
		ERROR("Can't open file \"" + filename + "\" for reading configurations");
	for (Configuration cfg; cfg.Load(ifs);)
		cfgs.emplace_back(cfg);
	return cfgs;
}
