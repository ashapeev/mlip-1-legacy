/*   This software is called MLIP for Machine Learning Interatomic Potentials.
 *   MLIP can only be used for non-commercial research and cannot be re-distributed.
 *   The use of MLIP must be acknowledged by citing approriate references.
 *   See the LICENSE file for details.
 *
 *   Contributors: Evgeny Podryabinkin
 */

#include "mlip_wrapper.h"
#include "mtp.h"
#include "linear_regression.h"
#ifdef DEV
#	include "../dev_src/mtpr_trainer.h"
#endif


using namespace std;


void MLIP_Wrapper::SetUpMLIP()
{
#ifdef DEV // developer's version
	MLMTPR* p_mtpr = nullptr;
	LinearRegression* p_linreg = nullptr;
#endif

	if (mlip_fnm.empty())
	{
		enable_EFScalc = false;
		enable_learn = false;
		enable_select = false;
		monitor_errs = false;
		Warning("No MLIP activated. Ab-initio model and driver will be linked directly");

		return;
	}
	else
	{
		// forward reading .mtp-file to find out the number of components of MTP
		int spec_count = 1;
		{
			ifstream ifs(mlip_fnm);
			if (!ifs.is_open())
				ERROR("Can't open file " + mlip_fnm + " for reading");
			while (!ifs.eof())
			{
				string foo;
				ifs >> foo;
				if (foo == "species_count")
				{
					ifs.ignore(999, '=');
					ifs >> spec_count;
					break;
				}
			}
		}
		// to decide mtp or MTPR instantiate
		if (spec_count == 1)
		{
			p_mlip = new MTP(mlip_fnm);
			Message("One-component MTP instantiated");
		}
		else
#ifdef DEV
		{
			p_mlip = p_mtpr = new MLMTPR(mlip_fnm);
			Message("Multycomponent MTP instantiated");
		}
#else
			ERROR("Public version has not support multicomponent MTP yet");
#endif
	}

	if (enable_learn)
	{
#ifdef DEV
		if (p_mtpr == nullptr)
			p_learner = p_linreg =
			new LinearRegression(p_mlip, fit_ene_wgt, fit_frc_wgt, fit_str_wgt, fit_rel_frc_wgt);
		else
			p_learner = new MTPR_trainer(	p_mtpr, 
											fit_ene_wgt, 
											fit_frc_wgt, 
											fit_str_wgt, 
											fit_rel_frc_wgt, 
											1.0e-6);
#else
		p_learner = new LinearRegression(	p_mlip, 
											fit_ene_wgt, 
											fit_frc_wgt, 
											fit_str_wgt, 
											fit_rel_frc_wgt);
#endif
		p_learner->SetLogStream(fit_log);
	}

	if (enable_select)
	{
		if (slct_trsh_init <= 0.0 || slct_trsh_slct <= 0.0)
			ERROR("Invalid threshold specified");
		if (slct_trsh_swap <= 0.0)
			slct_trsh_swap = slct_trsh_slct;

		p_selector = new MaxvolSelection(p_mlip,
			slct_trsh_init, slct_trsh_slct, slct_trsh_swap,
			slct_nbh_wgt, slct_ene_wgt, slct_frc_wgt, slct_str_wgt);
		p_selector->SetLogStream(slct_log);

		if (!slct_state_load_fnm.empty())
			p_selector->Load(slct_state_load_fnm);
	
		p_selector->SetLogStream(slct_log);
	}

	// lotf scenario initialization
	if (enable_EFScalc && enable_learn && enable_select)
	{
#ifdef DEV
		if (p_mtpr == nullptr)	
			p_lotf = new LOTF(p_abinitio, p_selector, p_linreg, lotf_EFSviaMTP);
		else
			p_lotf = new LOTF(p_abinitio, p_selector, p_learner, lotf_EFSviaMTP);
#else
		p_lotf = new LOTF(p_abinitio, p_selector, (LinearRegression*)p_learner, lotf_EFSviaMTP);
#endif
		p_lotf->SetLogStream(lotf_log);

		if (!slct_state_load_fnm.empty())	// important in case of loading selection witout learning
			p_learner->Train(p_selector->selected_cfgs);
	}

	if (monitor_errs)
	{
		if (p_abinitio == nullptr || !enable_EFScalc)
		{
			Warning("Unable to activate errors monitoring (invalid settings)");
			monitor_errs = false;
		}
		else
			errmon.SetLogStream(errmon_log);
		
		if (p_lotf != nullptr)
			p_lotf->collect_abinitio_cfgs = true;
	}

	// checking and cleaning output configurations file (it is better to crash with error in the beginning)
	if (!cfgs_fnm.empty())
	{
		ofstream ofs_tmp(cfgs_fnm);
		if (!ofs_tmp.is_open())
			ERROR("Can't open .cfgs file \"" + cfgs_fnm + "\" for output");
	}

	SetLogStream(log_output);
}

MLIP_Wrapper::MLIP_Wrapper(	map<string, string> settings, AnyPotential* _p_abinitio)
{
	Message("MLIP initialization");
	
	p_abinitio = _p_abinitio;

	InitSettings();
	ApplySettings(settings);
	PrintSettings();

	SetUpMLIP();

	if (((p_mlip == nullptr) && (p_abinitio == nullptr)) ||
		(enable_learn && (p_abinitio == nullptr)))
		ERROR("Incompatible setings");

	Message("MLIP initialization complete");
}

MLIP_Wrapper::~MLIP_Wrapper()
{
	logstrm << endl;	// to keep previous messages printed with '\r'

	if (monitor_errs)
	{
		ostream* tmp = SetStreamForOutput(nullptr); SetStreamForOutput(tmp);
		if (errmon.GetLogStream() != tmp)
			errmon.report(tmp);
		errmon.report(errmon.GetLogStream());
	}
	if (enable_select)
	{
		if (!slct_ts_fnm.empty())
			p_selector->SaveSelected(slct_ts_fnm);
		if (!slct_state_save_fnm.empty())
			p_selector->Save(slct_state_save_fnm);
	}

	if (enable_learn)
	{
		if (!enable_EFScalc)
			if (enable_select)	// EFS calculation with abinitio potential before training
			{
				logstrm << "MLIP: calculating EFS for selected configurations" << std::endl;
				int cntr=0;
				for (Configuration& cfg : p_selector->selected_cfgs)
				{
					p_abinitio->CalcEFS(cfg);
					logstrm << "MLIP: " << ++cntr << " processed\r";
					if (GetLogStream() != nullptr) GetLogStream()->flush();
				}
				p_learner->Train(p_selector->selected_cfgs);
			}
			else
				p_learner->Train(training_set);
		//else {}		// Training have already to be done in the other case

		if (!mlip_fitted_fnm.empty())
			p_mlip->Save(mlip_fitted_fnm);
	}

	if (p_selector != nullptr)
		delete p_selector;

	if (p_learner != nullptr)
		delete p_learner;

	if (p_mlip != nullptr)
		delete p_mlip;

	Message("MLIP object has been destroyed");
}

void MLIP_Wrapper::CalcEFS(Configuration & cfg)
{
	if (monitor_errs)	// This placed before CalcEFS(cfg) for correct work with VoidPotential
		cfg_valid = cfg;

	// Consider all possible combinations of enable_EFScalc, enable_learn, enable_select flags
	if (!enable_EFScalc && !enable_learn && !enable_select)
	{
		if (p_abinitio != nullptr)
			p_abinitio->CalcEFS(cfg);
	}
	else if (enable_EFScalc && !enable_learn && !enable_select)		// just EFS calculation mode
	{
		p_mlip->CalcEFS(cfg);

		if (monitor_errs)
		{
			p_abinitio->CalcEFS(cfg_valid);
			errmon.collect(cfg, cfg_valid);
		}
	}
	else if (!enable_EFScalc && enable_learn && !enable_select)	// Learn all configuration mode
	{
		if (p_abinitio != nullptr)
			p_abinitio->CalcEFS(cfg);
		else
			ERROR("Attempting learning on configurations with no EFS");

		training_set.push_back(cfg);
	}
	else if (!enable_EFScalc && !enable_learn && enable_select)	// Select MV-set mode
	{
		if (p_abinitio != nullptr)
			p_abinitio->CalcEFS(cfg);

		p_selector->Select(cfg);
	}
	else if (enable_EFScalc && enable_learn && !enable_select)	// Potential retrains and calculate EFS after adding each configuration (quite strange regime)
	{
		if (p_abinitio != nullptr)
		{
			p_abinitio->CalcEFS(cfg);
			cfg_valid = cfg;
		}
		else
			ERROR("Attempting learning on configurations with no EFS");

		training_set.push_back(cfg);
		p_learner->Train(training_set);

		p_mlip->CalcEFS(cfg);

		if (monitor_errs)
			errmon.collect(cfg, cfg_valid);
	}
	else if (enable_EFScalc && !enable_learn && enable_select)	// EFS calculation combined with selection in one run
	{
		p_mlip->CalcEFS(cfg);
		p_selector->Select(cfg);

		if (monitor_errs)
		{
			p_abinitio->CalcEFS(cfg_valid);
			errmon.collect(cfg, cfg_valid);
		}
	}
	else if (!enable_EFScalc && enable_learn && enable_select)	// Configurations are selecting while run, selected to be leaned in destructor. EFS are calculated with Ab-initio only for selected configurations 
	{
		if (p_abinitio != nullptr)
			p_abinitio->CalcEFS(cfg);

		p_selector->Select(cfg);
	}
	else if (enable_EFScalc && enable_learn && enable_select)	// LOTF scenario
	{
		int abinitio_clcs = p_lotf->pot_calcs_cntr;
		p_lotf->CalcEFS(cfg);

		if (monitor_errs)
			if (abinitio_clcs < p_lotf->pot_calcs_cntr) // abinitio EFS qurried
			{// No additional abinitio calcs. Take EFS for cfg_valid from accumulator
				cfg_valid = p_lotf->abinitio_cfgs.back();
				p_lotf->abinitio_cfgs.clear();
				errmon.collect(cfg_valid, cfg);
			}
			else
			{
				p_abinitio->CalcEFS(cfg_valid);
				errmon.collect(cfg_valid, cfg);
			}
	}

	if (!cfgs_fnm.empty() && (call_cntr % (skip_saving_N_cfgs+1) == 0))
		cfg.AppendToFile(cfgs_fnm);
	
	call_cntr++;
	logstrm << "MLIP: processed " << call_cntr << " configurations" << endl;
}

void MLIP_Wrapper::CalcE(Configuration & cfg)
{
	bool EFSfromAbInitio = false;

	if (enable_EFScalc || enable_learn || enable_select)
	{
		if (monitor_errs)	// This placed before CalcEFS(cfg) for correct work with VoidPotential
			cfg_valid = cfg;

		// Consider all possible combinations of enable_EFScalc, enable_learn, enable_select
		if (enable_EFScalc && !enable_learn && !enable_select)		// just E calculation mode
		{
			p_mlip->CalcE(cfg);
		}
		else if (!enable_EFScalc && enable_learn && !enable_select)	// Learn all configuration mode
		{
			if (p_abinitio != nullptr)
				p_abinitio->CalcEFS(cfg);
			else
				ERROR("Attempting learning on configurations with no EFS");

			training_set.push_back(cfg);
		}
		else if (!enable_EFScalc && !enable_learn && enable_select)	// Select MV-set mode
		{
			p_selector->Select(cfg);
		}
		else if (enable_EFScalc && enable_learn && !enable_select)	// Potential retrains and calculate EFS after adding each configuration
		{
			if (p_abinitio != nullptr)
				p_abinitio->CalcEFS(cfg);
			else
				ERROR("Attempting learning on configurations with no EFS");

			training_set.push_back(cfg);
			p_learner->Train(training_set);

			p_mlip->CalcE(cfg);
		}
		else if (enable_EFScalc && !enable_learn && enable_select)	// EFS calculation combined with selection in one run
		{
			p_mlip->CalcE(cfg);
			p_selector->Select(cfg);
		}
		else if (!enable_EFScalc && enable_learn && enable_select)	// Configurations are selecting while run, selected to be leaned in destructor. EFS are calculated with Ab-initio only for selected configurations 
		{
			p_selector->Select(cfg);
		}
		else if (enable_EFScalc && enable_learn && enable_select)	// LOTF scenario
			p_lotf->CalcEFS(cfg);

		if (monitor_errs)
		{
			if (!EFSfromAbInitio)
			{
				p_abinitio->CalcEFS(cfg_valid);
				errmon.collect(cfg_valid, cfg);
			}
			else
				errmon.collect(cfg, cfg);
		}
	}
	else if (p_abinitio != nullptr)
		p_abinitio->CalcEFS(cfg);

	if (!cfgs_fnm.empty() && (call_cntr % (skip_saving_N_cfgs+1) == 0))
		cfg.AppendToFile(cfgs_fnm);

	logstrm << "MLIP: processed " << ++call_cntr << " configurations" << endl;
}
