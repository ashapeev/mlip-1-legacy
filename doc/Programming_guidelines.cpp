// The documentation is written for "doxygen"
// download from http://ftp.stack.nl/pub/users/dimitri/doxygen-1.8.10-setup.exe
// or http://www.stack.nl/~dimitri/doxygen/download.html
//
// Load the Doxygen file, which is the configuration file
//  
// The output is written into /html (should we put it into .gitignore?)

/**
\mainpage MLIP Developer Documentation

\section coding Coding Conventions

\subsection coding_naming Naming Conventions

Files: `my_useful_class.cpp` or `my_useful_class.h`

Classes: `LinearRegression` or  `ExitStatus` (enum)

Variables:
```
local_variable
struct_data_member
class_data_member
pointers must have "p_" prefix
p_linear_system   
```

Constants:
```
const int DAYS_IN_A_WEEK = 7; (same as Enumerator names)
WeekDay::MONDAY
```

Functions: Regular functions have mixed case; "cheap" functions may use lower case with underscores.
```
AddTableEntry()
DeleteUrl()
bool is_empty()
```

Abbreviations: use only approved abbreviations:

- cfg = configuration
- pos = position, positions
- nbh = neighborhood
- vel = velocity
- wgt = weight
- cnt = count
- cntr = counter
- cfgs = configurations
- project/potential names: mlip, mtp, mtpr
- fnm = filename
- ene = energy
- frc = forces
- str = stresses
- eqtn = equation
- common abbreviations in the field: slae, rhs
    - Matrix regression_slae
    - SolveSlaeRegularized

\subsection coding_other Other:

```
#ifndef _MLIP_CONFIGURATION
#define _MLIP_CONFIGURATION
...
#endif // _MLIP_CONFIGURATION
```


\subsection formatting Formatting:

- line length = 100  (except comments, but comment should start within 100 simbols)

- tabulation: 4
- public/protected...: 2
```
class MyObject {
  public:
    MyObject...
  protected:
    ...
};
```
- #directives: left edge

\subsection indexing Indexing

- spatial dimensions: a, b (but vectorial operations are preferred)
- other indexes: i, j, ...

\subsection doxygen Comments according to doxygen

- be in the process of converting comments describing functions, members, etc., to the goxigen format
    - use `\\!` and `\\!<` (try to avoid `\*!`)

\subsection compiletime Compile Time Options

MLIP_DEBUG switches on some checks that can help to debug a newly written code

\section errors Errors and Warnings

For MLIP to wait for the Enter key to press in warnings and errors,
set an environment variable

    MLIP_WAIT_FOR_KEYPRESS=true

\section site_energy Site Energy

Site energy can be calculated in VASP as \f$E _\ell(x) = \sum _{i=1}^N ...\f$

*/
