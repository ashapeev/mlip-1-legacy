Execute:
$ $(Path_to_MLIP)/mlp run mlip.ini
this calculates errors of energy, forces, and stresses (EFS)
computed by MTP100_fitted.mtp as compared to DFT (in file Li_NVT300.cfg)
