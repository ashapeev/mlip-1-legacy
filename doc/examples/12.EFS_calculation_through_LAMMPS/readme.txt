Execute:
$ $(Path_to_MLIP)/mlp run mlip.ini
This calculates EFS for all configurations from database Li_T300.cfg with EAM potential (Li.eam.fs) available through LAMMPS.
All configurations are written to Li_EAM.cfg 

