Execute:
$ $(Path_to_MLIP)/mlp run mlip.ini
This trains the potential MTP100.mtp on configurations from the database Li_NVT300.cfg.
The trained potential is saved to out/MTP100_fitted.mtp
