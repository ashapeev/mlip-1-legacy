Execute:
$ lmp_mpi < lmp.inp
or
$ mpirun -n 8 lmp_mpi < lmp.inp
This example demonstrates the work of LAMMPS (parallel version) with linked MLIP.
MLIP is linked for treating a quite large configuration (30,000 atoms) by atomic neighborhoods.
