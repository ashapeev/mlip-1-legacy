Execute:
$ cp MTP100.mtp out/
$ mpiexec -n 4 $(Path_to_MLIP)/mlp train out/MTP100.mtp trainset.cfg
This fits out/MTP100.mtp on trainset.cfg on 4 mpi cores.
The result is saved to out/MTP100.mtp (the potential file is rewritten).
The weights of the fitting can be given as options.
