Execute:
$ $(Path_to_LAMMPS_with_MLIP)/lmp_serial < lmp.inp
This example demonstrates the work of LAMMPS (serial version) with active learning on the fly MTP (Active learning in "all equations" regime). 
MLIP learns DFT behavior provided by VASP.
