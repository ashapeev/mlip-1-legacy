This folder contains a number of examples of usage of mlp.
Typically, a folder contains a readme.txt file describing the example,
some input files, a sample_out folder with the expected output files,
and an empty out folder where the output files are saved.

TODO: create sample_out for examples 6,9,10,... and redirect output to out
