Execute:
$ $(Path_to_LAMMPS_with_MLIP)/lmp_serial < lmp.inp
This example demonstrates learning MTP (MTP100.mtp) on the fly while MD process driven by LAMMPS. 
Ab-intio model is EAM (Li.eam.fs) interfaced by LAMMPS.
Fitting errors are calculated while LOTF.
