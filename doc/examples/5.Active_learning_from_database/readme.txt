Execute:
$ $(Path_to_MLIP)/mlp run mlip.ini
this reads Li_NVT300.cfg, selects the configurations with active learning ("by neighborhoods"),
saves the selected configurations to out/TS100.cfg,
and fits MTP on them (the trained MTP is saved to out/MTP100_fitted.mtp)
