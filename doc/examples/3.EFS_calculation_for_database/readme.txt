Execute:
$ $(Path_to_MLIP)/mlp run mlip.ini
this calculates energy, forces and stresses (EFS) with MTP100_fitted.mtp
for all configurations from the database Li_NVT300.cfg, 
and saves configurations with this data to out/Li_EFSbyMTP.cfg
