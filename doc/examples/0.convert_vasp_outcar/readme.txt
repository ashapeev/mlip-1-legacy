Execute:
$ $(Path_to_MLIP)/mlip convert-cfg --input-format=vasp-outcar OUTCAR out/relax.cfg
This converts OUTCAR to the internal format out/relax.cfg.
Run
$ $(Path_to_MLIP)/mlip help convert-cfg
to see more options

