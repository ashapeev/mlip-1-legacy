Execute:
$ $(Path_to_LAMMPS_with_MLIP)/lmp_serial < lmp.inp
This example demonstrates ab-initio molecular dynamics with writing a configuration database. 
DFT by VASP is ab-initio model, LAMMPS (serial version) is MD driver.
Each tenth configuration is recorded.
Any MTP potential (foo.mtp) is required to enable writing configurations
