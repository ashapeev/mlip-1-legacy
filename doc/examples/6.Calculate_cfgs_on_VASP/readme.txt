Execute:
$ mlp run mlip.ini
NOTE: to run this example VASP is required. Path to VASP should be specified in run_vasp.sh script
This recalculates configurations from TS.cfg on VASP and saves them to TS_VASP.cfg.
To run example any MTP is required to be loaded (foo.mtp in this example)
