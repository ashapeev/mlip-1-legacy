make QUIET_MAKE=1 TARGET=NEHALEM DYNAMIC_ARCH=1 HOSTCC=gcc NUM_THREADS=64 BINARY=32 CC=i686-w64-mingw32-gcc FC=i686-w64-mingw32-gfortran
[INFO] : TIMER value: INT_ETIME (given by make.inc)
[INFO] : TIMER value: INT_ETIME (given by make.inc)
touch libopenblasp-r0.2.19.a
make -s -j 16 -C test all
├── bin
│   └── libopenblas.dll       The shared library for Visual Studio and GCC.
├── include
│   ├── cblas.h
│   ├── f77blas.h
│   ├── lapacke_config.h
│   ├── lapacke.h
│   ├── lapacke_mangling.h
│   ├── lapacke_utils.h
│   └── openblas_config.h
├── lib
│   ├── libopenblas.a         The static library. Only work with GCC.
│   └── libopenblas.dll.a     The import library for Visual Studio.
└── readme.txt
