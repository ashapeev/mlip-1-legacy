# Contributions to MLIP

## Lead developers

* Alexander Shapeev <a.shapeev@skoltech.ru>
* Evgeny Podryabinkin <e.podryabinkin@skoltech.ru>
* Ivan Novikov <i.novikov@skoltech.ru>

## Developers:

* Konstantin Gubaev <Konstantin.Gubaev@skoltech.ru>
  * MTPR (multi-component potentials)

## Code contributors

## Performance report contributors
* Ivan Novoselov
* Conrad Rosenbrock
* Livia Bartok-Partay

## Acknowledgements

This project is supported by the Skoltech NGP Program No. 2016-7/NGP (a Skoltech-MIT joint project).
